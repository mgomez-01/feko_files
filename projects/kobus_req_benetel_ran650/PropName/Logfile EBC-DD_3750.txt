Start Day and Time: 2023-11-13 at 18:38:07 (-0800)

2023-11-13 at 18:38:07 (-0800): Init Network Data
2023-11-13 at 18:38:07 (-0800): Init random data generator
2023-11-13 at 18:38:07 (-0800): Init network results
2023-11-13 at 18:38:07 (-0800): Prepare paremeters
2023-11-13 at 18:38:07 (-0800): *****************************************************************************************************
2023-11-13 at 18:38:07 (-0800): Prepare parameters of network project started...
2023-11-13 at 18:38:07 (-0800): *****************************************************************************************************
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800): Preparation of transmitters for prediction started...
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800):     Set pointer from transmitters to sites started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Set pointer from transmitters to sites done
2023-11-13 at 18:38:07 (-0800):     Determine min. antenna height at each site started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Determine min. antenna height at each site done
2023-11-13 at 18:38:07 (-0800):     Determine apertures of antennas started...
2023-11-13 at 18:38:07 (-0800):         Aperture of antennna "EBC-DD_3750" is 360.00�
2023-11-13 at 18:38:07 (-0800):     Determine apertures of antennnas done
2023-11-13 at 18:38:07 (-0800):     Remove disabled transmitters from list
2023-11-13 at 18:38:07 (-0800):     Copy default settings of transmitters started
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Copy default settings of transmitters done
2023-11-13 at 18:38:07 (-0800):     Check azimuth of antenna patterns started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Check azimuth of antenna patterns done
2023-11-13 at 18:38:07 (-0800):     Define prediction areas started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Define prediction areas done
2023-11-13 at 18:38:07 (-0800):     Check power settings started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Check power settings done
2023-11-13 at 18:38:07 (-0800):     Tramsmitters counted (1 transmitters)
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800): Preparation of transmitters for prediction completed
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800): *****************************************************************************************************
2023-11-13 at 18:38:07 (-0800): Prepare parameters of network project completed
2023-11-13 at 18:38:07 (-0800): *****************************************************************************************************
2023-11-13 at 18:38:07 (-0800): Check the results to be computed
2023-11-13 at 18:38:07 (-0800): Init structures for results
2023-11-13 at 18:38:07 (-0800): Start with propagation predictions...
2023-11-13 at 18:38:07 (-0800): **************************************************************************************
2023-11-13 at 18:38:07 (-0800):    Checking settings of transmitter "EBC-DD_3750"....
2023-11-13 at 18:38:07 (-0800): **************************************************************************************
2023-11-13 at 18:38:07 (-0800): **************************************************************************************
2023-11-13 at 18:38:07 (-0800):    Checking settings of transmitter     "EBC-DD_3750"   done
2023-11-13 at 18:38:07 (-0800):      => Transmitter is ok
2023-11-13 at 18:38:07 (-0800): **************************************************************************************
2023-11-13 at 18:38:07 (-0800):  **************************************************************************************************************
2023-11-13 at 18:38:07 (-0800):    EBC-DD_3750 
2023-11-13 at 18:38:07 (-0800):  **************************************************************************************************************
2023-11-13 at 18:38:07 (-0800):  => Start with prediction of transmitter/antenna/cell
2023-11-13 at 18:38:07 (-0800):  **************************************************************************************************************
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Rays.str" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Rays.ray" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Field Strength.fpf" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Power.fpp" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "MS Results\EBC-DD_3750\Power.fpp" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Path Loss.fpl" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "MS Results\EBC-DD_3750\Delay Spread.fpd" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Delay Time.fpt" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\LOS.los" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (BS).amb" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (MS).amm" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (BS).emb" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (MS).emm" deleted because of re-computation
2023-11-13 at 18:38:07 (-0800): Start of Urban Computation
2023-11-13 at 18:38:07 (-0800): Init data done
2023-11-13 at 18:38:07 (-0800): Check project parameters started....
2023-11-13 at 18:38:07 (-0800):   Checking Parameters
2023-11-13 at 18:38:07 (-0800):   Checking Lines
2023-11-13 at 18:38:07 (-0800):   Copy data of default TRX and remove default TRX from list
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800): Preparation of transmitters for prediction started...
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800):     Check properties of new transmitter
2023-11-13 at 18:38:07 (-0800):     Check properties of default transmitter
2023-11-13 at 18:38:07 (-0800):     Set pointer from transmitters to sites started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Set pointer from transmitters to sites done
2023-11-13 at 18:38:07 (-0800):     Determine min. antenna height at each site started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Determine min. antenna height at each site done
2023-11-13 at 18:38:07 (-0800):     Determine apertures of antennas started...
2023-11-13 at 18:38:07 (-0800):         Aperture of antennna "EBC-DD_3750" is 360.00�
2023-11-13 at 18:38:07 (-0800):     Determine apertures of antennnas done
2023-11-13 at 18:38:07 (-0800):     Check azimuth of antenna patterns started...
2023-11-13 at 18:38:07 (-0800):     Check azimuth of antenna patterns done
2023-11-13 at 18:38:07 (-0800):     Define prediction areas started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Define prediction areas done
2023-11-13 at 18:38:07 (-0800):     Check power settings started...
2023-11-13 at 18:38:07 (-0800):         Antenna "EBC-DD_3750"
2023-11-13 at 18:38:07 (-0800):     Check power settings done
2023-11-13 at 18:38:07 (-0800):     Tramsmitters counted (1 transmitters)
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800): Preparation of transmitters for prediction completed
2023-11-13 at 18:38:07 (-0800): ****************************************************************
2023-11-13 at 18:38:07 (-0800):   Check TRX settings
2023-11-13 at 18:38:07 (-0800):   Check computation mode
2023-11-13 at 18:38:07 (-0800):   Check delay spread settings
2023-11-13 at 18:38:07 (-0800):   Check default material properties
2023-11-13 at 18:38:07 (-0800): Computation of transmission matrix only possible if Fresnel/UTD model and no postprocessing is selected.
2023-11-13 at 18:38:07 (-0800): Therefore the prediction of transmission matrix is disabled!
2023-11-13 at 18:38:07 (-0800):   Check prediction area
2023-11-13 at 18:38:07 (-0800):   Check transition between IRT and COST postprocessing
2023-11-13 at 18:38:07 (-0800):   Checks successful => Continue with further computation!
2023-11-13 at 18:38:07 (-0800): Check project parameters done.
2023-11-13 at 18:38:07 (-0800): Copy Project Parameters started....
2023-11-13 at 18:38:07 (-0800):    Copy location of transmitters
2023-11-13 at 18:38:07 (-0800):    Copy further parameters started...
2023-11-13 at 18:38:07 (-0800):        Copy further parameters
2023-11-13 at 18:38:07 (-0800):        Copy settings of area mode
2023-11-13 at 18:38:07 (-0800):        Copy breakpoint settings (2.600, 3.300)
2023-11-13 at 18:38:07 (-0800):        Copy settings of postprocessing
2023-11-13 at 18:38:07 (-0800):        Copy settings of empirical indoor coverage
2023-11-13 at 18:38:07 (-0800):        Copy CNP settings
2023-11-13 at 18:38:07 (-0800):        Copy parameters done
2023-11-13 at 18:38:07 (-0800):    Copy further parameters done.
2023-11-13 at 18:38:07 (-0800):    Copy parameters successfully completed.
2023-11-13 at 18:38:07 (-0800): Copy Project Parameters done.
2023-11-13 at 18:38:07 (-0800): Preparation of antenna patttern of "EBC-DD_3750" started....
2023-11-13 at 18:38:07 (-0800): Read antenna pattern "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\antennas\VVSSP-360S-F\VVSSP-360S-F_SbandCombined_00DT_3750"
2023-11-13 at 18:38:07 (-0800): Antenna pattern "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\antennas\VVSSP-360S-F\VVSSP-360S-F_SbandCombined_00DT_3750.msi"
2023-11-13 at 18:38:07 (-0800): successfully read from disk!
2023-11-13 at 18:38:07 (-0800): Preparation of antenna patttern of "EBC-DD_3750" completed
2023-11-13 at 18:38:07 (-0800): Calling dedicated urban propagation module...
2023-11-13 at 18:38:07 (-0800):     Urban Propagation Model started...
2023-11-13 at 18:38:07 (-0800):     Init coordinate system
2023-11-13 at 18:38:07 (-0800):     Init prediction area
2023-11-13 at 18:38:07 (-0800):     Init interface
2023-11-13 at 18:38:07 (-0800):     Calling propagation engine ...
2023-11-13 at 18:38:07 (-0800): Starting with computation of transmitter "EBC-DD_3750"
2023-11-13 at 18:38:08 (-0800): Different coordinate ellipsoid in building and topo database defined!
2023-11-13 at 18:38:08 (-0800):   => Determination of absolute height of buildings (incl. topography) started...
2023-11-13 at 18:38:09 (-0800):   => Determination of absolute height of buildings (incl. topography) completed
2023-11-13 at 18:38:09 (-0800): DPM Release +++ Version 2022.3.2-28592
2023-11-13 at 18:38:09 (-0800):   0% of prediction with Dominant Path Model done
2023-11-13 at 18:38:10 (-0800): Buildings raster initialized (1.00 m resolution).
2023-11-13 at 18:38:10 (-0800): Allocation of memory for basic matrix started..
2023-11-13 at 18:38:10 (-0800):    -> Layer 1: 1.50 m
2023-11-13 at 18:38:10 (-0800): Allocation of memory for basic matrix successfully finished.
2023-11-13 at 18:38:10 (-0800): Initialization of adaptive resolution started..
2023-11-13 at 18:38:10 (-0800):  10% of prediction with Dominant Path Model done
2023-11-13 at 18:38:10 (-0800): Initialization of adaptive resolution successfully finished.
2023-11-13 at 18:38:10 (-0800): Initialization of pixels started..
2023-11-13 at 18:38:10 (-0800): Initialization of pixels successfully finished.
2023-11-13 at 18:38:10 (-0800): Generation of building raster matrix started..
2023-11-13 at 18:38:10 (-0800): Generation of building raster matrix successfully completed.
2023-11-13 at 18:38:10 (-0800): Computation of dominant paths started..
2023-11-13 at 18:38:10 (-0800):  30% of prediction with Dominant Path Model done
2023-11-13 at 18:38:11 (-0800):  40% of prediction with Dominant Path Model done
2023-11-13 at 18:38:12 (-0800):  50% of prediction with Dominant Path Model done
2023-11-13 at 18:38:12 (-0800):  60% of prediction with Dominant Path Model done
2023-11-13 at 18:38:14 (-0800):  70% of prediction with Dominant Path Model done
2023-11-13 at 18:38:14 (-0800):  80% of prediction with Dominant Path Model done
2023-11-13 at 18:38:15 (-0800): Computation of dominant paths successfully finished.
2023-11-13 at 18:38:15 (-0800): Prediction of rays with DPM submodel 1 finished.
2023-11-13 at 18:38:15 (-0800): Interpolation of result data started..
2023-11-13 at 18:38:15 (-0800):  90% of prediction with Dominant Path Model done
2023-11-13 at 18:38:15 (-0800): Interpolation of result data successfully finished.
2023-11-13 at 18:38:15 (-0800): Preparation of results started..
2023-11-13 at 18:38:16 (-0800): Preparation of results successfully finished.
2023-11-13 at 18:38:16 (-0800): Free of memory started..
2023-11-13 at 18:38:16 (-0800): Free of memory successfully finished.
2023-11-13 at 18:38:16 (-0800): DPM module finished.
2023-11-13 at 18:38:16 (-0800): LOS computation started...
2023-11-13 at 18:38:16 (-0800):     Compute LOS status for each pixel in result
2023-11-13 at 18:38:16 (-0800):     Status of outdoor pixels:
2023-11-13 at 18:38:16 (-0800):         LOS      23069 pixel
2023-11-13 at 18:38:16 (-0800):         NLOS     98976 pixel
2023-11-13 at 18:38:16 (-0800):         VLOS     4172 pixel
2023-11-13 at 18:38:16 (-0800):         VNLOS    82673 pixel
2023-11-13 at 18:38:16 (-0800):         Total   208890 pixel
2023-11-13 at 18:38:16 (-0800): LOS computation completed.
2023-11-13 at 18:38:16 (-0800):     Propagation engine successfully completed
2023-11-13 at 18:38:16 (-0800):     Free memory started...
2023-11-13 at 18:38:16 (-0800): Free memory for locations
2023-11-13 at 18:38:16 (-0800):     Free memory completed.
2023-11-13 at 18:38:16 (-0800): Close Files
2023-11-13 at 18:38:16 (-0800): Urban Module finished successfully
2023-11-13 at 18:38:16 (-0800): Returning from dedicated urban propagation module.
2023-11-13 at 18:38:16 (-0800): Prepare internal postprocessing of data
2023-11-13 at 18:38:16 (-0800): Writing results....
2023-11-13 at 18:38:16 (-0800): Results written.
2023-11-13 at 18:38:16 (-0800): Urban Propagation Module successfully completed
2023-11-13 at 18:38:16 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Field Strength.txt"....
2023-11-13 at 18:38:17 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Field Strength.txt" successfully completed!
2023-11-13 at 18:38:17 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Field Strength.fpf"....
2023-11-13 at 18:38:17 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Field Strength.fpf" successfully completed!
2023-11-13 at 18:38:17 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Power.txt"....
2023-11-13 at 18:38:17 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Power.txt" successfully completed!
2023-11-13 at 18:38:17 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Power.fpp"....
2023-11-13 at 18:38:17 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Power.fpp" successfully completed!
2023-11-13 at 18:38:17 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Path Loss.txt"....
2023-11-13 at 18:38:18 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Path Loss.txt" successfully completed!
2023-11-13 at 18:38:18 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Path Loss.fpl"....
2023-11-13 at 18:38:18 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Path Loss.fpl" successfully completed!
2023-11-13 at 18:38:18 (-0800):         Computation of Delay Spread started...
2023-11-13 at 18:38:18 (-0800):         Computation of Delay Spread completed!
2023-11-13 at 18:38:18 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Delay Time.txt"....
2023-11-13 at 18:38:18 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Delay Time.txt" successfully completed!
2023-11-13 at 18:38:18 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Delay Time.fpt"....
2023-11-13 at 18:38:18 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Delay Time.fpt" successfully completed!
2023-11-13 at 18:38:18 (-0800):         Computation of Angular Means started ...
2023-11-13 at 18:38:18 (-0800):         Computation of Angular Means completed!
2023-11-13 at 18:38:18 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (BS).txt"....
2023-11-13 at 18:38:19 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (BS).txt" successfully completed!
2023-11-13 at 18:38:19 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (BS).amb"....
2023-11-13 at 18:38:19 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (BS).amb" successfully completed!
2023-11-13 at 18:38:19 (-0800):         Computation of Angular Means started ...
2023-11-13 at 18:38:19 (-0800):         Computation of Angular Means completed!
2023-11-13 at 18:38:19 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (BS).txt"....
2023-11-13 at 18:38:19 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (BS).txt" successfully completed!
2023-11-13 at 18:38:19 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (BS).emb"....
2023-11-13 at 18:38:19 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (BS).emb" successfully completed!
2023-11-13 at 18:38:19 (-0800):         Computation of Angular Means started ...
2023-11-13 at 18:38:19 (-0800):         Computation of Angular Means completed!
2023-11-13 at 18:38:19 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (MS).txt"....
2023-11-13 at 18:38:19 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (MS).txt" successfully completed!
2023-11-13 at 18:38:19 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (MS).amm"....
2023-11-13 at 18:38:19 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Angular Mean (MS).amm" successfully completed!
2023-11-13 at 18:38:19 (-0800):         Computation of Angular Means started ...
2023-11-13 at 18:38:20 (-0800):         Computation of Angular Means completed!
2023-11-13 at 18:38:20 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (MS).txt"....
2023-11-13 at 18:38:20 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (MS).txt" successfully completed!
2023-11-13 at 18:38:20 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (MS).emm"....
2023-11-13 at 18:38:20 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\Elevation Mean Angle (MS).emm" successfully completed!
2023-11-13 at 18:38:20 (-0800):         Computation of LOS status started ...
2023-11-13 at 18:38:20 (-0800):         Computation of LOS status completed!
2023-11-13 at 18:38:20 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\LOS.txt"....
2023-11-13 at 18:38:20 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\LOS.txt" successfully completed!
2023-11-13 at 18:38:20 (-0800): Writing file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\LOS.los"....
2023-11-13 at 18:38:20 (-0800): Write file "Y:\RA_Stuff\projects\kobus_req_benetel_ran650\PropName\EBC-DD_3750\LOS.los" successfully completed!
2023-11-13 at 18:38:20 (-0800):         Writing of str-file started ...
2023-11-13 at 18:38:22 (-0800):         Writing of str-file completed!
2023-11-13 at 18:38:22 (-0800):         Writing of ray-file started ...
2023-11-13 at 18:38:22 (-0800):         Writing of ray-file completed!
2023-11-13 at 18:38:22 (-0800): Free parameters of urban project started...
2023-11-13 at 18:38:22 (-0800):     - free geometry of prediction area
2023-11-13 at 18:38:22 (-0800):     - free transmitter information
2023-11-13 at 18:38:22 (-0800):     - free CNP parameters
2023-11-13 at 18:38:22 (-0800): Free parameters of urban project completed
2023-11-13 at 18:38:22 (-0800):  **************************************************************************************************************
2023-11-13 at 18:38:22 (-0800):  => Prediction of transmitter (antenna)
2023-11-13 at 18:38:22 (-0800):     "EBC-DD_3750" completed!!
2023-11-13 at 18:38:22 (-0800):  **************************************************************************************************************
2023-11-13 at 18:38:22 (-0800):  **************************************************************************************************************
2023-11-13 at 18:38:22 (-0800): Propagation predictions completed

Stop Day and Time: 2023-11-13 at 18:38:22 (-0800)
