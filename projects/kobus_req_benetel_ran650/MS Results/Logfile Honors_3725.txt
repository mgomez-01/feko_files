Start Day and Time: 2023-11-13 at 18:47:16 (-0800)

2023-11-13 at 18:47:16 (-0800): *****************************************************************************************************************
2023-11-13 at 18:47:16 (-0800): Computation Engine started...
2023-11-13 at 18:47:16 (-0800): Version 2022.3.2-28592
2023-11-13 at 18:47:16 (-0800): *****************************************************************************************************************
2023-11-13 at 18:47:16 (-0800): Computations started: 2023-11-13, 10:47:16 (-0800)
2023-11-13 at 18:47:16 (-0800): ****************************************************************
2023-11-13 at 18:47:16 (-0800): Preparation of transmitters for prediction started...
2023-11-13 at 18:47:16 (-0800): ****************************************************************
2023-11-13 at 18:47:16 (-0800):     Check properties of new transmitter
2023-11-13 at 18:47:16 (-0800):     Check properties of default transmitter
2023-11-13 at 18:47:16 (-0800):     Set pointer from transmitters to sites started...
2023-11-13 at 18:47:16 (-0800):         Antenna "Honors_3725"
2023-11-13 at 18:47:16 (-0800):     Set pointer from transmitters to sites done
2023-11-13 at 18:47:16 (-0800):     Determine min. antenna height at each site started...
2023-11-13 at 18:47:16 (-0800):         Antenna "Honors_3725"
2023-11-13 at 18:47:16 (-0800):     Determine min. antenna height at each site done
2023-11-13 at 18:47:16 (-0800):     Determine apertures of antennas started...
2023-11-13 at 18:47:16 (-0800):         Aperture of antennna "Honors_3725" is 360.00�
2023-11-13 at 18:47:16 (-0800):     Determine apertures of antennnas done
2023-11-13 at 18:47:16 (-0800):     Check azimuth of antenna patterns started...
2023-11-13 at 18:47:16 (-0800):         Antenna "Honors_3725"
2023-11-13 at 18:47:16 (-0800):     Check azimuth of antenna patterns done
2023-11-13 at 18:47:16 (-0800):     Define prediction areas started...
2023-11-13 at 18:47:16 (-0800):         Antenna "Honors_3725"
2023-11-13 at 18:47:16 (-0800):     Define prediction areas done
2023-11-13 at 18:47:16 (-0800):     Check power settings started...
2023-11-13 at 18:47:16 (-0800):         Antenna "Honors_3725"
2023-11-13 at 18:47:16 (-0800):     Check power settings done
2023-11-13 at 18:47:16 (-0800):     Tramsmitters counted (1 transmitters)
2023-11-13 at 18:47:16 (-0800): ****************************************************************
2023-11-13 at 18:47:16 (-0800): Preparation of transmitters for prediction completed
2023-11-13 at 18:47:16 (-0800): ****************************************************************
2023-11-13 at 18:47:17 (-0800): *****************************************************************************************************************
2023-11-13 at 18:47:17 (-0800): Computation Engine started...
2023-11-13 at 18:47:17 (-0800): Version 2022.3.2-28592
2023-11-13 at 18:47:17 (-0800): *****************************************************************************************************************
2023-11-13 at 18:47:17 (-0800): Computations started: 2023-11-13, 10:47:17 (-0800)
2023-11-13 at 18:47:17 (-0800): Initialisation of structures.
2023-11-13 at 18:47:17 (-0800): Reading antenna data for Tx array started...
2023-11-13 at 18:47:17 (-0800): Reading antenna data for Tx array successfully finished.
2023-11-13 at 18:47:17 (-0800): Initialisation of trajectory data started...
2023-11-13 at 18:47:17 (-0800):     -> Changing resolution of ray matrix started...
2023-11-13 at 18:47:17 (-0800):     -> Changing resolution of ray matrix successfully finished.
2023-11-13 at 18:47:17 (-0800): Initialisation of trajectory data successfully finished.
2023-11-13 at 18:47:17 (-0800): Calculation of channel matrices started... 
2023-11-13 at 18:47:17 (-0800):     -> Memory allocation for channel matrices started...
2023-11-13 at 18:47:17 (-0800):     -> Memory allocation for channel matrices successfully finished.
2023-11-13 at 18:47:18 (-0800): Calculation of channel matrices successfully finished.
2023-11-13 at 18:47:18 (-0800): Writing Rx power levels to ASCII file started...
2023-11-13 at 18:47:18 (-0800): MS Results\Honors_3725\Sub-Channel Rx-1 Power.txt
2023-11-13 at 18:47:19 (-0800): Writing Rx power levels to ASCII file successfully finished.
2023-11-13 at 18:47:19 (-0800): Superposition of channel matrices started...
2023-11-13 at 18:47:19 (-0800): Superposition of channel matrices successfully finished.
2023-11-13 at 18:47:19 (-0800): Generating of resulting ray matrices started...
2023-11-13 at 18:47:20 (-0800): Generating of resulting ray matrices successfully finished.
2023-11-13 at 18:47:20 (-0800): Writing channel matrices to ASCII file started...
2023-11-13 at 18:47:20 (-0800): MS Results\Honors_3725\ChannelMatricesPerPoint.txt
2023-11-13 at 18:47:21 (-0800): Writing channel matrices to ASCII file successfully finished.
2023-11-13 at 18:47:21 (-0800): Calculation of channel condition numbers started...
2023-11-13 at 18:47:21 (-0800): Writing channel condition numbers to ASCII file started...
2023-11-13 at 18:47:21 (-0800): Writing channel condition numbers to ASCII file successfully finished.
2023-11-13 at 18:47:21 (-0800): Writing channel condition number to WinProp result file started...
2023-11-13 at 18:47:21 (-0800): Writing channel condition number to WinProp result file successfully finished.
2023-11-13 at 18:47:21 (-0800): Writing ray data to ASCII file started...
2023-11-13 at 18:47:21 (-0800): MS Results\Honors_3725/
2023-11-13 at 18:47:21 (-0800):         Writing of str-file started ...
2023-11-13 at 18:47:22 (-0800):         Writing of str-file completed!
2023-11-13 at 18:47:22 (-0800): MS Results\Honors_3725\Sub-Channel Rx-1 
2023-11-13 at 18:47:22 (-0800):         Writing of str-file started ...
2023-11-13 at 18:47:23 (-0800):         Writing of str-file completed!
2023-11-13 at 18:47:23 (-0800): MS Results\Honors_3725\Stream 
2023-11-13 at 18:47:23 (-0800):         Writing of str-file started ...
2023-11-13 at 18:47:24 (-0800):         Writing of str-file completed!
2023-11-13 at 18:47:24 (-0800): Writing ray data to ASCII file successfully finished.
2023-11-13 at 18:47:24 (-0800): Writing ray data to WinProp result file started...
2023-11-13 at 18:47:24 (-0800): MS Results\Honors_3725\Rays
2023-11-13 at 18:47:24 (-0800):         Writing of ray-file started ...
2023-11-13 at 18:47:25 (-0800):         Writing of ray-file completed!
2023-11-13 at 18:47:25 (-0800): MS Results\Honors_3725\Sub-Channel Rx-1 Rays
2023-11-13 at 18:47:25 (-0800):         Writing of ray-file started ...
2023-11-13 at 18:47:25 (-0800):         Writing of ray-file completed!
2023-11-13 at 18:47:25 (-0800): MS Results\Honors_3725\Stream Rays
2023-11-13 at 18:47:25 (-0800):         Writing of ray-file started ...
2023-11-13 at 18:47:26 (-0800):         Writing of ray-file completed!
2023-11-13 at 18:47:26 (-0800): Writing ray data to WinProp result file successfully finished.
2023-11-13 at 18:47:26 (-0800): Writing ray data to ASCII file started...
2023-11-13 at 18:47:26 (-0800): MS Results\Honors_3725\RayProfile.txt
2023-11-13 at 18:47:26 (-0800): Writing ray data to ASCII file successfully finished.
2023-11-13 at 18:47:26 (-0800): Diversity combining started...
2023-11-13 at 18:47:26 (-0800):     -> Memory allocation for diversity combining started...
2023-11-13 at 18:47:26 (-0800):     -> Memory allocation for diversity combining successfully finished.
2023-11-13 at 18:47:27 (-0800): Diversity combining successfully finished.
2023-11-13 at 18:47:27 (-0800): Writing diversity combining results to ASCII file started...
2023-11-13 at 18:47:27 (-0800): MS Results\Honors_3725\DiversityCombining.txt
2023-11-13 at 18:47:28 (-0800): Writing diversity combining results to ASCII file successfully finished.
2023-11-13 at 18:47:28 (-0800): Writing Rx power levels to ASCII file started...
2023-11-13 at 18:47:28 (-0800): MS Results\Honors_3725\Power.txt
2023-11-13 at 18:47:28 (-0800): Writing Rx power levels to ASCII file successfully finished.
2023-11-13 at 18:47:28 (-0800): Writing received power to WinProp result file started...
2023-11-13 at 18:47:28 (-0800): MS Results\Honors_3725\Power.fpp
2023-11-13 at 18:47:28 (-0800): Writing received power to WinProp result file successfully finished.
2023-11-13 at 18:47:28 (-0800): Free allocated memory for diversity combining started...
2023-11-13 at 18:47:28 (-0800): Free allocated memory for diversity combining successfully finished.
2023-11-13 at 18:47:28 (-0800): Writing received power to WinProp result file started...
2023-11-13 at 18:47:29 (-0800): MS Results\Honors_3725\Sub-Channel Rx-1 Power.fpp
2023-11-13 at 18:47:29 (-0800): Writing received power to WinProp result file successfully finished.
2023-11-13 at 18:47:29 (-0800): Writing received power to WinProp result file started...
2023-11-13 at 18:47:29 (-0800): MS Results\Honors_3725\Stream Power.fpp
2023-11-13 at 18:47:29 (-0800): Writing received power to WinProp result file successfully finished.
2023-11-13 at 18:47:29 (-0800): Writing Rx power levels to ASCII file started...
2023-11-13 at 18:47:29 (-0800): MS Results\Honors_3725\Stream Power.txt
2023-11-13 at 18:47:30 (-0800): Writing Rx power levels to ASCII file successfully finished.
2023-11-13 at 18:47:30 (-0800): Calculation of delay spread started...
2023-11-13 at 18:47:30 (-0800): Calculation of delay spread successfully finished.
2023-11-13 at 18:47:30 (-0800): Writing delay spread to ASCII file started...
2023-11-13 at 18:47:30 (-0800): MS Results\Honors_3725\Delay Spread.txt
2023-11-13 at 18:47:30 (-0800): Writing delay spread to ASCII file successfully finished.
2023-11-13 at 18:47:30 (-0800): Writing delay spread to WinProp result file started...
2023-11-13 at 18:47:30 (-0800): MS Results\Honors_3725\Delay Spread.fpd
2023-11-13 at 18:47:30 (-0800): Writing delay spread to WinProp result file successfully finished.
2023-11-13 at 18:47:30 (-0800): Calculation of angular spread started...
2023-11-13 at 18:47:30 (-0800): Calculation of angular spread successfully finished.
2023-11-13 at 18:47:30 (-0800): Writing angular spread to ASCII file started...
2023-11-13 at 18:47:30 (-0800): MS Results\Honors_3725\AngularSpread.txt
2023-11-13 at 18:47:31 (-0800): Writing angular spread to ASCII file successfully finished.
2023-11-13 at 18:47:31 (-0800): Writing angular spread to WinProp result file started...
2023-11-13 at 18:47:31 (-0800): MS Results\Honors_3725\Angular Spread (BS).asb
2023-11-13 at 18:47:31 (-0800): MS Results\Honors_3725\Angular Spread (MS).asm
2023-11-13 at 18:47:31 (-0800): MS Results\Honors_3725\Elevation Spread (BS).esb
2023-11-13 at 18:47:31 (-0800): MS Results\Honors_3725\Elevation Spread (MS).esm
2023-11-13 at 18:47:31 (-0800): Writing angular spread to WinProp result file successfully finished.
2023-11-13 at 18:47:31 (-0800): Writing angular means to ASCII file started...
2023-11-13 at 18:47:31 (-0800): MS Results\Honors_3725\AngularMeans.txt
2023-11-13 at 18:47:32 (-0800): Writing angular means to ASCII file successfully finished.
2023-11-13 at 18:47:32 (-0800): Writing angular means to WinProp result file started...
2023-11-13 at 18:47:32 (-0800): MS Results\Honors_3725\Angular Mean (BS).amb
2023-11-13 at 18:47:32 (-0800): MS Results\Honors_3725\Angular Mean (MS).amm
2023-11-13 at 18:47:32 (-0800): MS Results\Honors_3725\Elevation Mean Angle (BS).emb
2023-11-13 at 18:47:32 (-0800): MS Results\Honors_3725\Elevation Mean Angle (MS).emm
2023-11-13 at 18:47:32 (-0800): Writing angular means to WinProp result file successfully finished.
2023-11-13 at 18:47:32 (-0800): Calculation of estimated power azimuth spectrum at Rx array started...
2023-11-13 at 18:47:33 (-0800): Calculation of estimated power azimuth spectrum at Rx array successfully finished.
2023-11-13 at 18:47:33 (-0800): Writing power azimuth spectrum to ASCII file started...
2023-11-13 at 18:47:33 (-0800): MS Results\Honors_3725\PowerAzimuthSpectrum_Rx.txt
2023-11-13 at 18:47:33 (-0800): Writing power azimuth spectrum to ASCII file successfully finished.
2023-11-13 at 18:47:33 (-0800): Calculation of estimated power azimuth spectrum at Tx array started...
2023-11-13 at 18:47:33 (-0800): Calculation of estimated power azimuth spectrum at Tx array successfully finished.
2023-11-13 at 18:47:33 (-0800): Writing power azimuth spectrum to ASCII file started...
2023-11-13 at 18:47:33 (-0800): MS Results\Honors_3725\PowerAzimuthSpectrum_Tx.txt
2023-11-13 at 18:47:33 (-0800): Writing power azimuth spectrum to ASCII file successfully finished.
2023-11-13 at 18:47:33 (-0800): Normalisation of channel matrices started...
2023-11-13 at 18:47:33 (-0800):     -> Fast Fourier transformation of channel matrices started...
2023-11-13 at 18:47:36 (-0800):     -> Fast Fourier transformation of channel matrices successfully finished.
2023-11-13 at 18:47:36 (-0800):     -> Normalisation for same SNIR.
2023-11-13 at 18:47:36 (-0800): Normalisation of channel matrices successfully finished.
2023-11-13 at 18:47:36 (-0800): Calculation of channel capacity started... 
2023-11-13 at 18:47:36 (-0800):     -> Memory allocation for channel capacity started...
2023-11-13 at 18:47:36 (-0800):     -> Memory allocation for channel capacity successfully finished.
2023-11-13 at 18:47:38 (-0800): Calculation of channel capacity successfully finished.
2023-11-13 at 18:47:38 (-0800): Writing channel capacity to ASCII file started...
2023-11-13 at 18:47:38 (-0800): MS Results\Honors_3725\ChannelCapacity.txt
2023-11-13 at 18:47:39 (-0800): Writing channel capacity to ASCII file successfully finished.
2023-11-13 at 18:47:39 (-0800): Writing channel capacity to WinProp result file started...
2023-11-13 at 18:47:39 (-0800): MS Results\Honors_3725\Channel Capacity.cap
2023-11-13 at 18:47:39 (-0800): Writing channel capacity to WinProp result file successfully finished.
2023-11-13 at 18:47:39 (-0800): Computations finished: 2023-11-13, 10:47:39 (-0800)
2023-11-13 at 18:47:39 (-0800): *****************************************************************************************************************
2023-11-13 at 18:47:39 (-0800): Free allocated memory for channel matrices started...
2023-11-13 at 18:47:41 (-0800): Free allocated memory for channel matrices successfully finished.

Stop Day and Time: 2023-11-13 at 18:47:41 (-0800)
