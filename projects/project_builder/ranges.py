import numpy as np
from rich import print

# Given values
min_Pout = 37 - 2.5  # Minimum output power in dBm
max_Pout = 37 + 2.5  # Maximum output power in dBm

min_G = 5.6  # Minimum antenna gain in dBi
max_G = 6.0  # Maximum antenna gain in dBi

# Calculate minimum and maximum ERP
min_ERP = min_Pout + min_G
max_ERP = max_Pout + max_G

print(f"min_ERP:{min_ERP}")
print(f"max_ERP:{max_ERP}")


# Define the range of output power and antenna gain with increments
Pout_range = np.linspace(min_Pout, max_Pout, 6)  # 0.5 dBm increments
G_range = np.linspace(min_G, max_G, 5)  # 0.1 dBi increments

# Create a matrix to store all possible ERP values
ERP_matrix = np.zeros((len(Pout_range), len(G_range)))

# Calculate ERP for each combination
for i, Pout in enumerate(Pout_range):
    for j, G in enumerate(G_range):
        ERP_matrix[i, j] = Pout + G

#display the ERP matrix
print(f"ERP_matrix:\n{ERP_matrix}")
