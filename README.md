# FEKO CMD Line interface now working!
- I managed to get it working in linux and I believe I can do the same on the CADE Labs machines
- TODO:  Get with J. Pickering to install the correct set of libs and add in the append script bits to append to LD_LIBRARY_PATH to link the libs to the correct place. then try executing 
```
WinPropCLI --help
```
If you see the following 
```
feko_files (main) [!?] 
🚀 WinPropCLI --help
Altair WinProp - Command Line Interface, Version 2022-14829* (2022-03-31 16:09:57)
Options:
  --help                      Print help messages
  -F [ --file-project ] arg   Project file to compute
  --dynamic-limit arg (=-1)   Limit the dynamic of base station antenna 
                              patterns
  --filter-results arg (=-1)  Use arithmetic filter for prediction results 
                              during computation
  --multi-threading arg (=-1) Use multiple threads for computation
  -P [ --run-pro ]            Compute the propagation
  -M [ --run-ms ]             Compute mobile station post processing
  -N [ --run-net ]            Compute the network planning
  -A [ --run-all ]            Compute all in the project enabled modes (same as
                              "--run-pro --run-ms --run-net")
  --results-pro arg           Results directory for propagation, if empty, 
                              results directory from project is taken. Absolute
                              or relative to project file. Used for output of 
                              "--run-pro" and as input of "--run-ms" and 
                              "--run-net"
  --results-ms arg            Results directory for postprocessing, if empty, 
                              results directory from project is taken. Absolute
                              or relative to project file. Used for output of 
                              "--run-ms" and as input of "--run-net"
  --results-net arg           Results directory for network planning, if empty,
                              results directory from project is taken. Absolute
                              or relative to project file.
  --disable-project-update    Disable updating ProMan project files after 
                              computation.
  -I [ --process-db ] arg     Database preprocessing. The expected argument is 
                              the path (including file extension) of a *.pin or
                              *.pre file for Indoor or Urban preprocessing 
                              respectively.
  -D [ --db-out ] arg         Name of resulting database, without extension. 
                              Using name of input file in case this parameter 
                              is omitted.
  -S [ --show-progress ] arg  Show progress information mode (detailed, global,
                              or auto).
  -R [ --convert-res ] arg    Binary result to be converted into WinProp ASCII 
                              format. The expected argument is the path 
                              (including file extension) of the binary result 
                              file.
  --res-out arg               Name of resulting converted result, without 
                              extension. Using name of input file in case this 
                              parameter is omitted.
  -V [ --version ]            Print the Altair WinProp version


feko_files (main) [!?] 
🚀 

```
Then were in business. 


# feko_files - The last place for all your feko-simulation file needs
All files needed for making a campus map in feko and adding in antennas from the included msi or txt files
*IMPORTANT* must export license path var prior to running scripts that use feko
``` bash
  if [ `(echo $ALTAIR_WINPROP) | grep winprop` ] ;
  then echo "winprop dir already set up";
  else export ALTAIR_WINPROP="~/2022/altair/feko/api/winprop/";
  fi

  export ALTAIR_LICENSE_PATH='6200@winlic-d.eng.utah.edu'
```
- this is the way I was able to get the two working. the above ALTAIR_WINPROP is for the CMakeLists.txt and the ALTAIR_LICENSE_PATH is for the server to be called instead of running locally.
  - in order for the above to work as intended, you will have to use the GlobalProtect VPN client


## Getting started
Launch feko and create a new project
- need 
  - campus map
  - Map_UofU_campus.osm
  - and databases 
  - data too large for the github lfs. will ignore until a better way is figured out
- scripting api doc for winprop to stitch things together
- making the map in wallman
  - open wallman and go to File > convert urban db > vectordb
  - add in the map_UofU_campus_outputfile.osm to map 
  - utm zone 12 N
  - save db
- make topodb
  - go to File > convert topo DB 
  - Select GeoTIFF
  - utm zone 12 N
  - check the box to make .tdv (useful later)
  - interpolate undef. vals
  - give a name and save the topo db
- Net file
  - contains all files and params for setting up a project. 

## Goal of this repo
- The plan for this is to be the last place to go when wanting to make a model in feko. 
  - eventually, I want to be able to say I want Route A on campus with antenna A on BS(base), antenna B on MS(mobile) each with their params, with base settings(prop only, no network stuff)
	- this will make the net file for feko postman for you and, ideally, open postman for you with computations run
	
### Roadmap
- [X] base files built for use in sims
- [ ] pipeline for those to be run by daemon (completed-ish) still needs some robustness for exec loop(handle in queue?)
		
### Notes for Miguel 
- [x] completed conversion of osm map for feko programatically with C++
- [x] completed map exporting using bakemap.py (I accidentally called it bakemap instead of makemap but I liked it and decided it can stay)
- Call it like this

``` bash
	python3 bakemap.py -D -f ../power_files_dir/
```
- Notes for bakemap:
	- this expects the files it will process to be in the format exported by feko in txt
	- -D flag is to set debugging to see printout
	- -f used to specify the path or file you want to convert. 
	- outputs two files, one filename_map.png and filename_data.tif
		- tif file is in WGS84 by default. functionality for UTM already exists and will add a switch in the future for -P(rojection) UTM or WGS
	
- [x] ~~next is create a project net file with C++~~ No longer priority as cli is functional
- [ ] Add in copying of proj.mic/net/nup/wpi/wst/... etc. to complete project builder in both windows and linux
- [ ] Find a better way to programatically include dir for antennas 

### Add trajectories files
- [x] Load the csvs in bus_routes, or add the coordinates file
- [ ] Routes pulled and working as expected. add in headings to the same dataset.
  - in progress Combination can be done in the current calculation code. only need to take the heading for that point and do the same with it. 

# Known bugs
- TODO: Fix the path linking for antennas in project creation for both windows and linux creator. path still works in RA_Stuff proj, check there for the fix since it is possible the versions were not -ff properly on the last cloning
