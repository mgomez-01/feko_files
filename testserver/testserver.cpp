#include <algorithm>
#include <arpa/inet.h>
#include <iostream>
#include <map>
#include <netinet/in.h>
#include <string>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <vector>
#include <algorithm> 
#include <cctype>
#include <locale>

// Trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
        return !std::isspace(ch);
    }));
}

// Trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// Function to trim the string (remove leading and trailing whitespaces)
std::string trim(const std::string& str) {
    size_t first = str.find_first_not_of(" \t\n\r");
    size_t last = str.find_last_not_of(" \t\n\r");
    if (first == std::string::npos || last == std::string::npos)
        return "";
    return str.substr(first, last - first + 1);
}

// Trim from start (copying)
static inline std::string ltrim_copy(std::string s) {
    ltrim(s);
    return s;
}

// Trim from end (copying)
static inline std::string rtrim_copy(std::string s) {
    rtrim(s);
    return s;
}

// Trim from both ends (copying)
static inline std::string trim_copy(std::string s) {
    trim(s);
    return s;
}



enum class KnownParams { PROGRAM, ARGS, FILE };
std::string get_json_value(const std::string &json_str, const std::string &key);

std::map<std::string, KnownParams> str_to_enum = {
    {"program", KnownParams::PROGRAM},
    {"args", KnownParams::ARGS},
    {"file", KnownParams::FILE}};

std::string handle_client_request(const std::string &json_str,
                                  std::vector<std::string> &params) {
  std::string response;
  std::string unknown_param;
  bool all_params_known = true;

  size_t pos = 0;
  while ((pos = json_str.find(':', pos)) != std::string::npos) {
    size_t start_quote = json_str.rfind('"', pos);
    size_t end_quote = json_str.rfind('"', start_quote - 1);
    std::string param =
        json_str.substr(end_quote + 1, start_quote - end_quote - 1);
    params.push_back(param);

    if (str_to_enum.find(param) == str_to_enum.end()) {
      unknown_param = param;
      all_params_known = false;
      break;
    }
    pos++;
  }

  if (all_params_known) {
    if (std::find(params.begin(), params.end(), std::string("file")) !=
        params.end()) {
      std::cout << "Debug: JSON String before calling get_json_value: " << json_str << std::endl;
      // problem when testing with python test server. file not found in that case but works with curl
      // curl -d '{"program":"my_program","args":"some_args", "file":"testing"}' -H 'Content-Type: application/json' localhost:8080
      std::string filename = get_json_value(json_str, "file");
if (filename.empty()) {
    response = "{\"error\":\"malformed or missing filename\"}";
} else {
    std::string file_url = "http://localhost/home/speedy/repos/feko_files/testserver/" + filename + ".json";
    response = "{\"file_url\":\"" + file_url + "\"}";
    printf("file_url: %s\n", file_url.c_str());
}
      printf("filename: %s\n", filename.c_str());
      printf("response: %s\n", response.c_str());

    } else {
      response = "{\"success\":\"all parameters are sound\"}";
    }
  } else {
    response = "{\"error\":\"parameter " + unknown_param + " unknown\"}";
  }
  printf("response: %s\n", response.c_str());

  return response;
}

std::string get_json_value(const std::string& json_str_raw, const std::string& key) {
    // Trim the string
    std::string json_str = trim(json_str_raw);

    // Prepare the key to find
    std::string key_to_find = "\"" + key + "\":\"";

    // Find the starting index of the value
    size_t start = json_str.find(key_to_find);
    
    // Check if the key was found
    if (start == std::string::npos) {
        std::cout << "Key not found: " << key << std::endl;
        return "";
    }

    // Calculate the starting position of the value
    start += key_to_find.length();

    // Find the ending index of the value
    size_t end = json_str.find("\"", start);

    // Extract and return the value
    return json_str.substr(start, end - start);
}


int read_full_http_request(int new_socket, std::string& http_request) {
    char buffer[1024] = {0};
    int bytes_read = read(new_socket, buffer, 1024);
    if (bytes_read <= 0) {
        return -1;
    }

    http_request = std::string(buffer, bytes_read);

    // Find the Content-Length header
    size_t pos = http_request.find("Content-Length: ");
    if (pos == std::string::npos) {
        return -1;
    }

    // Extract the content length value
    std::string content_length_str = http_request.substr(pos + 16, http_request.find("\r\n", pos) - (pos + 16));
    int content_length = std::stoi(content_length_str);

    // Find the start of the JSON payload
    size_t json_start = http_request.find("\r\n\r\n");
    if (json_start == std::string::npos) {
        return -1;
    }

    // Calculate how many payload bytes have already been read
    int payload_bytes_read = bytes_read - (json_start + 4);

    // Read the remaining payload content
    while (payload_bytes_read < content_length) {
        bytes_read = read(new_socket, buffer, 1024);
        if (bytes_read <= 0) {
            return -1;
        }
        http_request += std::string(buffer, bytes_read);
        payload_bytes_read += bytes_read;
    }

    return 0;
}

int main() {
    int server_fd = socket(AF_INET, SOCK_STREAM, 0);
    struct sockaddr_in address;
    int addrlen = sizeof(address);

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(8080);

    bind(server_fd, (struct sockaddr *)&address, sizeof(address));
    listen(server_fd, 3);

    while (true) {
        int new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen);

        std::string http_request;
        if (read_full_http_request(new_socket, http_request) == 0) {
            // Now, http_request contains the full HTTP request, both headers and payload.

            // Find the start of the JSON payload
            size_t json_start = http_request.find("\r\n\r\n");
            if (json_start != std::string::npos) {
                std::string json_str = http_request.substr(json_start + 4);  // Skip "\r\n\r\n"

                std::vector<std::string> params;
		printf("%s\n",json_str.c_str());
                std::string response_str = handle_client_request(json_str, params);

                std::string http_response = "HTTP/1.1 200 OK\r\n"
                                            "Content-Type: application/json\r\n"
                                            "Content-Length: " + std::to_string(response_str.length()) +
                                            "\r\n\r\n" + response_str;

                send(new_socket, http_response.c_str(), http_response.length(), 0);
            }
        }

        close(new_socket);
    }

    return 0;
}
