import requests
import json
import sys
import os

# Replace with the server's actual IP and port
server_url = "http://127.0.0.1:8888"
print(os.path.abspath('.'))
# JSON payload
payload = json.dumps({"program": "WinPropCLI", "args": "--help", "file": "USTAR-Power", "params": {"site1": {"power": 30, "carrierID": 1}}})
print(payload)
# Send a POST request to the server
response = requests.post(server_url, headers={"Content-Type": "application/json"}, data=payload)
print(response)
# Parse the JSON response
json_response = response.json()
print("Raw Response:", response.content)

# If the server response contains a file_url, download the file
if "file_url" in json_response:
    file_url = json_response["file_url"]
    print(f"file_url: {file_url}")
    file_name = file_url.split('/')[-1]
    print(f"file_name: {file_name}")
    if file_name[0] == '.':
        print("malformed filename: exiting")
        sys.exit(0)
    if not file_url.endswith('.json'):
        print("Invalid file URL received:", file_url)
        # Handle error, maybe exit or continue
        sys.exit(0)
    file_response = requests.get(file_url)
    # print(file_response.content)
    # Save the file
    with open("downloaded_file.json", "wb") as f:
        f.write(file_response.content)

    # # Read the file and print its contents (as a demonstration)
    # with open("downloaded_file.json", "r") as f:
    #     data = json.load(f)
    #     print("File contents:", json.dumps(data, indent=4))
