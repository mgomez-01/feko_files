import socket
from http.server import SimpleHTTPRequestHandler
import socketserver
import subprocess
import json
from enum import Enum
import logging
from libs.customFormatter import CustomFormatter
import threading
import argparse
import os

logger = CustomFormatter.createLogger('Job server:', './logs/python_server.log')
CustomFormatter.setLogLevel(logger, logging.DEBUG)


# Enumerated known parameters
class KnownParams(Enum):
    PROGRAM = "program"
    ARGS = "args"
    FILE = "file"
    PARAMS = "params"


# Map from string to enum
str_to_enum = {
    "program": KnownParams.PROGRAM,
    "args": KnownParams.ARGS,
    "file": KnownParams.FILE,
    "params": KnownParams.PARAMS
}

data_dir = "data_precompute/"
FS_PORT = 8899


def validate_and_handle_params(request_json):
    response = {}
    unknown_param = None
    all_params_known = True
    
    for param in request_json.keys():
        if param not in str_to_enum:
            unknown_param = param
            all_params_known = False
            break
        
    if all_params_known:
        if "file" in request_json:
            filename = request_json["file"]
            if not filename:
                response["error"] = "malformed or missing filename"
            else:
                file_url = f"http://localhost:{FS_PORT}/{data_dir}{filename if filename.endswith('.json') else filename + '.json'}"
                response["file_url"] = file_url
        if "program" in request_json and "args" in request_json:
            program = request_json["program"]
            args = request_json["args"]
            proj_name = request_json.get("proj_name", "")
            logger.debug(f"program {program} seen in request, executing {program} with args: {args} for proj: {proj_name}")
            try:
                # Run the program and wait for it to complete
                # completed_process = subprocess.run([program, args], capture_output=True, text=True, check=True)
                completed_process = subprocess.run([program, args], capture_output=True, text=True, check=False, env=os.environ, shell=True)
                program_output = completed_process.stdout
                logger.debug(f"Program_output:{program_output}")
                # Parse output for keywords if needed
                if "Altair" in program_output:
                    response["keyword_found"] = True
                    logger.debug("Altair seen in resulting output")
                    response["program_output"] = program_output
            except subprocess.CalledProcessError as e:
                response["error"] = f"Program execution failed: {str(e)}"
                logger.debug(f"Run attempt:{[program, args]} failed")
                logger.debug(f"\nerror: {str(e)}")
        else:
            response["success"] = "all parameters are sound"
    else:
        response["error"] = f"parameter {unknown_param} unknown"
    
    return json.dumps(response)


def read_full_http_request(client_socket):
    buffer = b""
    while True:
        chunk = client_socket.recv(1024)
        if not chunk:
            break
        buffer += chunk
        if b"\r\n\r\n" in buffer:
            break
    
    headers, payload = buffer.split(b"\r\n\r\n", 1)
    headers = headers.decode("utf-8")
    
    # Extract content length
    content_length = int(headers.split("Content-Length: ")[1].split("\r\n")[0])
    
    # Read remaining payload if necessary
    while len(payload) < content_length:
        payload += client_socket.recv(1024)
    
    return payload.decode("utf-8")


def handle_client_request(http_request):
    # Parse JSON payload
    request_json = json.loads(http_request)

    # Validate known parameters (to be implemented)
    response_str = validate_and_handle_params(request_json)
    
    # Prepare HTTP response
    http_response = f"HTTP/1.1 200 OK\r\n" \
                    f"Content-Type: application/json\r\n" \
                    f"Content-Length: {len(response_str)}\r\n\r\n{response_str}"
    
    return http_response


def start_fileserver(Handler):
    def run_file_server():
        logger.debug(f"File server is running at port {FS_PORT}")
        file_server.serve_forever()
    file_server = socketserver.TCPServer(("", FS_PORT), Handler)
    t = threading.Thread(target=run_file_server)
    t.daemon = True  # Daemon threads exit when the program exits
    t.start()


def main():
    # Create an ArgumentParser object to define command-line arguments
    parser = argparse.ArgumentParser(description="Server Configuration")

    # Add arguments for ADDRESS and PORT
    parser.add_argument("--address", type=str, default="localhost", help="Server address")
    parser.add_argument("--port", type=int, default=8080, help="Server port")

    # Parse the command-line arguments
    args = parser.parse_args()
    
    # Use args.address and args.port in your code
    ADDRESS = args.address
    PORT = args.port
    # Create a socket object
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    
    # Bind the socket to address and port
    server_socket.bind((ADDRESS, PORT))
     
   # Listen for incoming connections (maximum 3 clients in the waiting queue)
    server_socket.listen(3)
    Handler = SimpleHTTPRequestHandler
    Handler.directory = data_dir
    start_fileserver(Handler)
    while True:
        # Accept a connection from a client
        client_socket, client_address = server_socket.accept()
        
        # Read the HTTP request from the client (to be implemented)
        http_request = read_full_http_request(client_socket)
        logger.debug(f"http_request:\n{http_request}")
        if http_request:
            # Handle the client request and prepare the HTTP response (to be implemented)
            http_response = handle_client_request(http_request)
            
            # Send the HTTP response back to the client
            client_socket.sendall(http_response.encode("utf-8"))
        
        # Close the client socket
        client_socket.close()


if __name__ == "__main__":
    main()
