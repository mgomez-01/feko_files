#!/usr/bin/env python3

import laspy
import matplotlib.pyplot as plt
import numpy as np
import pyvista as pv
import torch as torch
from scipy.spatial import cKDTree
import pandas as pd
import sys


if len(sys.argv) < 3:
    print("needs lidar filedata in .las format as arg1 and csv file with format containing at least the following headers as argv2:\nStation\t\tUTM_Easting\t\tUTM_Northing\n")
    exit()
elif not sys.argv[1].endswith('las'):
    print('needs lidar filedata in .las format as arg1')
    exit()
elif not sys.argv[2].endswith('csv'):
    print('csv file must have format containing at least the following headers as argv2:\nStation\t\tUTM_Easting\t\tUTM_Northing\n')
    exit()

lasfile = sys.argv[1]
csvfile = sys.argv[2]
# Read the .laz file
inFile = laspy.read(lasfile)

# Get the scale factors and offsets
scale_x, scale_y, scale_z = inFile.header.scale
offset_x, offset_y, offset_z = inFile.header.offset
pf = inFile.point_format
# Get the raw integer coordinates
X_raw, Y_raw, Z_raw = inFile.points['X'], inFile.points['Y'], inFile.points['Z']
R_raw, G_raw, B_raw = np.floor((inFile.points['red']/65536)*255), np.floor((inFile.points['green']/65536)*255), np.floor((inFile.points['blue']/65536)*255)
# Compute the real-world coordinates
X = X_raw * scale_x + offset_x
Y = Y_raw * scale_y + offset_y
Z = Z_raw * scale_z + offset_z
# Build a KD-tree
tree = cKDTree(np.c_[X, Y])

# Define a function to calculate the building elevation
def calculate_building_elevation(name, easting, northing, tree, Z, radius=.5):
    # Query the tree for points within the radius
    indices = tree.query_ball_point([easting, northing], radius)
    
    # Get the Z values of these points
    Z_building = Z[indices]
    
    # Calculate the building elevation
    elevation = np.max(Z_building)  # This assumes that the Z values represent elevation above sea level
    
    return elevation

# Load the building data
building_data = pd.read_csv(csvfile)  # Assumes the file is named 'building_data.csv'

# Initialize an empty list to store the results
results = []

# Iterate over the rows of the building data
for index, row in building_data.iterrows():
    # Calculate the building elevation
    elevation = calculate_building_elevation(row['Station'], row['UTM_Easting'], row['UTM_Northing'], tree, Z)
    
    # Append the result to the list
    results.append({'Station': row['Station'], 'Elevation(m)': elevation})

# Convert the results to a DataFrame
results = pd.DataFrame(results)

# Print the results
print(results)
