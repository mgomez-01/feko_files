import pandas as pd
from pyproj import Proj, transform

# Load the data in from the antenna location dataset
df = pd.read_csv('coordinates.csv')

# Function to determine UTM zone based on longitude in data
def get_utm_zone(lon):
    return int(1 + (lon + 180.0) / 6.0)

# Function to get the EPSG code for UTM conversion based on UTM zone and latitude. 
def get_utm_epsg(zone, lat):
    if lat >= 0:
        return 32600 + zone
    else:
        return 32700 + zone

# Function to apply the transformation
def convert_coordinates(row):
    lon, lat = row['Longitude'], row['Latitude']
    utm_zone = get_utm_zone(lon)
    utm_epsg = get_utm_epsg(utm_zone, lat)
    
    p1 = Proj(init='epsg:4326')  # WGS84
    p2 = Proj(init=f'epsg:{utm_epsg}') # UTM

    x, y = transform(p1, p2, lon, lat)
    return pd.Series((x, y, utm_zone))


routes = df.Description.unique()
grouped = df.groupby(df['Description'])
filename=f"{routes[0].replace(' ','_').lower()}_route.csv"
# Apply the function and add results to new columns
#df[['UTM_Easting', 'UTM_Northing', 'UTM_Zone']] = df.apply(convert_coordinates, axis=1)

# Save the result
for route in routes:
    if filename.find('.') != -1:
        filename=route.replace('.', '')
    else: 
        filename=route
    filename=f"{filename.replace(' ','_').lower()}_route.csv"
    print(filename)
    newframe=grouped.get_group(route)
    newframe[['UTM_Easting', 'UTM_Northing', 'UTM_Zone']] = newframe.apply(convert_coordinates, axis=1)
    print(f"saving data to {filename}:\ndata:",newframe)
    newframe.to_csv(filename, index=False)
