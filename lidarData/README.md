# Lidar data files
All files in the repo are .laz files due to the large size of the data. These can be decompressed using [LAStools](https://github.com/LAStools/LAStools)

Follow instructions on git for setting up lastools, and dont forget to add the bin dir inside to the path!

