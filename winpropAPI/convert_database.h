#pragma once

#include <../Interface/Engine.h>
#include <../Interface/EngineConvert.h>

int _STD_CALL CallbackMessage(const char *Text);
int _STD_CALL CallbackProgress(int value, const char* text);
int _STD_CALL CallbackError(const char *Message, int Mode);

int do_site_things();
