#include <iostream>
#include <stdio.h>
#include <string>
#include <unordered_map>

#include "convert_database.h"
#include "outdoor_propagation.h"



#ifndef API_DATA_FOLDER
#define API_DATA_FOLDER "../api_data"
#endif // !API_DATA_FOLDER

int main(int argc, char **argv) {
  std::cout << "in main" << std::endl;
  /* Define callback functions. */
  WinProp_Callback Callback;
  Callback.Percentage = CallbackProgress;
  Callback.Message = CallbackMessage;
  Callback.Error = CallbackError;
  
  /* Set correct parameters for conversion. */
  WinProp_Converter WinPropConverter;
  WinProp_Structure_Init_Converter(&WinPropConverter);
  std::cout << "Setting up structure." << std::endl;
  std::cout << WinPropConverter.ConverterID << std::endl;
  WinPropConverter.databaseNameSource =
      API_DATA_FOLDER "../mapdata/map_UofU_campus_outputfile.osm"; // with file extension
  WinPropConverter.databaseNameDest = API_DATA_FOLDER "outdoor/mapUofU_odb";
  WinPropConverter.measurementUnit = "Meter";
  WinPropConverter.ConverterID = 216; // Id for conversion of osm file
  WinPropConverter.SaveAsciiFormat = 0; // Save output database in binary format

  // Start conversion of data now: my_oda_Database.oda will be converted and
  // saved as output_Database_Path/my_oda_Database.odb.
  std::cout << "Trying conversion." << std::endl;
  if (WinProp_Convert(&WinPropConverter, &Callback) != 0)
    std::cout << "\nError during conversion" << std::endl;

  std::cout << "Conversion successful." << std::endl;
  std::cout << "Adding in sites." << std::endl;
  int res = do_site_things();
  if(!res)
    std::cout << "\nError during conversion." << std::endl;
  else
    std::cout << "\nSite things done successfully." << std::endl;
  return 0;
}

int do_site_things()
{
	int                 Error = 0;
	WinProp_ParaMain    GeneralParameters;
	WinProp_Antenna     Antenna;
	WinProp_Callback    Callback;
	WinProp_Result      Resultmatrix;
	WinProp_RayMatrix   RayMatrix;
	WinProp_Propagation_Results OutputResults;

	// name of .opb database without extensions
	const char* my_opb_database = API_DATA_FOLDER "outdoor/City";

	/* ------------------------ Initialisation of parameters ----------------------*/
	WinProp_Structure_Init_ParameterMain(&GeneralParameters);
	WinProp_Structure_Init_Antenna(&Antenna);
	WinProp_Structure_Init_Result(&Resultmatrix);
	WinProp_Structure_Init_RayMatrix(&RayMatrix);
	WinProp_Structure_Init_Propagation_Results(&OutputResults);

	/*---------------- Definition of scenario -------------------------------------*/
	/* Definition of general parameters. */
	GeneralParameters.ScenarioMode = SCENARIOMODE_URBAN; // Urban prediction
	GeneralParameters.PredictionModelUrban = PREDMODEL_UDP; // Use Dominant Path Model

	/* Definition of prediction area. */
	GeneralParameters.UrbanLowerLeftX = 100.0;
	GeneralParameters.UrbanLowerLeftY = 100.0;
	GeneralParameters.UrbanUpperRightX = 2200.0;
	GeneralParameters.UrbanUpperRightY = 3200.0;

	/* Copy coordinates to prediction area of second model (not yet used) */
	GeneralParameters.RuralLowerLeftX = GeneralParameters.UrbanLowerLeftX;
	GeneralParameters.RuralLowerLeftY = GeneralParameters.UrbanLowerLeftY;
	GeneralParameters.RuralUpperRightX = GeneralParameters.UrbanUpperRightX;
	GeneralParameters.RuralUpperRightY = GeneralParameters.UrbanUpperRightY;

	double PredictionHeight = 1.5;

	/* Size of matrix with results. */
	GeneralParameters.Resolution = 10.0;                     // Resolution in meter
	GeneralParameters.NrLayers = 1;                          // Number of prediction heights
	GeneralParameters.PredictionHeights = &PredictionHeight; // Prediction height in meter

	/* Building vector data and topography. */
	GeneralParameters.BuildingsMode = BUILDINGSMODE_UDP; // load a .opb database (urban + topography)
	sprintf(GeneralParameters.BuildingsName, "%s", my_opb_database); // Vector database in WinProp format

	/*--------------------------- Definition of antenna ---------------------------*/
	/* Position and configuration. */
	Antenna.Id = 1;
	Antenna.Longitude_X = 1100.0;
	Antenna.Latitude_Y = 2100.0;
	Antenna.Height = 20.0;
	Antenna.Power = 43.0; // Power in dBm
	Antenna.Frequency = 1800.0; // Frequency in MHz
	char AntennaName[500];
	sprintf(AntennaName, "%s", "Antenna 1");
	Antenna.Name = AntennaName;

	/* Definition of outputs to be computed and written in WinProp format. */
	GeneralParameters.OutputResults = &OutputResults;
	OutputResults.ResultPath = API_DATA_FOLDER "output/OutdoorArea_Output"; // Output data directory 
	OutputResults.FieldStrength = 1;
	OutputResults.PathLoss = 1;
	OutputResults.StatusLOS = 1;
	OutputResults.RayFilePropPaths = 1;
	OutputResults.StrFilePropPaths = 1;

	/*-------------------------- Callbacks ----------------------------------------*/
	Callback.Percentage = CallbackProgress;
	Callback.Message = CallbackMessage;
	Callback.Error = CallbackError;

	/*----------------------- Compute outdoor prediction --------------------------*/
	Error = OutdoorPlugIn_ComputePrediction(
		&Antenna, &GeneralParameters, NULL, NULL, NULL, NULL, &Callback, &Resultmatrix, &RayMatrix, NULL, NULL);
	
	/*----------------------- Do something with results ---------------------------*/
	if (Error == 0) {
		const char* Filename = API_DATA_FOLDER "output/Results.txt";
		write_ascii(&Resultmatrix, Filename);
	}
	else {
		/* Error during prediction. Print error message. */
		CallbackError(GeneralParameters.ErrorMessageMain, Error);
	}

	/*----------------------------- Free memory -----------------------------------*/
	WinProp_FreeResult(&Resultmatrix);
	WinProp_FreeRayMatrix(&RayMatrix);
	
	return 0;
}

int _STD_CALL CallbackMessage(const char *Text) {
  if (Text == nullptr)
    return 0;

  std::cout << "\n" << Text;

  return (0);
}

int _STD_CALL CallbackError(const char *Text, int Error) {
  if (Text == nullptr)
    return 0;

  std::cout << "\n";

#ifdef __LINUX
  std::cout << "\033[31m"
            << "Error (" << Error << "): "; // highlight error in red color
#else
  HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
  SetConsoleTextAttribute(hConsole, FOREGROUND_RED);
  std::cout << "Error (" << Error << "): ";
#endif // __LINUX
  std::cout << Text;

#ifdef __LINUX
  std::cout << "\033[0m"; // highlight error in red color
#else
  SetConsoleTextAttribute(hConsole,
                          FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
#endif // __LINUX

  return 0;
}

int _STD_CALL CallbackProgress(int value, const char *text) {
  char Line[200];

  sprintf(Line, "\n%d%% %s", value, text);
  std::cout << Line;

  return (0);
}
void write_ascii(const WinProp_Result* Resultmatrix, const char* Filename)
{
	FILE* OutputFile = fopen(Filename,"w");
	if (OutputFile)
	{
		/* Loop all pixels. */
		for (int x = 0; x < Resultmatrix->Columns; x++)
		{
			for (int y = 0; y < Resultmatrix->Lines; y++)
			{
				/* Compute real coordinates. */
				double Coordinate_X = Resultmatrix->LowerLeftX + ((double)x + 0.5) * Resultmatrix->Resolution;
				double Coordinate_Y = Resultmatrix->LowerLeftY + ((double)y + 0.5) * Resultmatrix->Resolution;

				/* Check if pixel was computed or not */
				if (Resultmatrix->Matrix[0][x][y] > -1000)
					fprintf(OutputFile, "%.2f\t%.2f\t%.2f\n", Coordinate_X, Coordinate_Y, Resultmatrix->Matrix[0][x][y]);
			}
		}

		/* Close file. */
		fclose(OutputFile);
	}
	else
		printf("\nCould not open the File: %s for writing.\n",Filename);
}
