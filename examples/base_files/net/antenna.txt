
*******************************************************************************
****** DEFINITION OF TRANSMITTERS *********************************************
*******************************************************************************
*******************************************************************************
***** Basestation / Antenna / Cell    No   1 **********************************
*******************************************************************************
***** Antenna modified since last path loss computation ***********************
ANTENNA N MODIFIED n n
***** Antenna enabled *********************************************************
ANTENNA N DISABLED n
***** Type of antenna (single antenna, radiating cable, repeater, etc.) *******
* Single Antenna = 0, Radiating Cable = 3, Repeater = 4,... 
ANTENNA N ANTENNA_TYPE 0
***** Coordinates of the antenna      [x,y,z] *********************************
ANTENNA N POSITION 429266.6700000000, 4513230.8600000003, 11.3000000000 
***** Name (ID) of the antenna / cell *****************************************
ANTENNA N NAME "_ANT_NAME"
***** Number of Site to which antenna belongs (-1 if no site) *****************
ANTENNA N SITE_NUMBER N
***** Antenna type (ISO, SECTOR) *********************************************
ANTENNA N TYPE SECTOR
***** Antenna pattern (Filename without extension) ****************************
ANTENNA N PATTERN_VERTICAL "_ANT_PATTERN_PATH"
ANTENNA N PATTERN_TYPE 4
***** Gain of Antenna *********************************************************
ANTENNA N ANTENNA_GAIN_VERTICAL 5.028
***** Horizontal Orientation of the antenna [Degrees] *************************
ANTENNA N HORIZONTAL 0.00 N
***** Vertical Orientation of the antenna (Downtilt) [Degrees] ****************
ANTENNA N VERTICAL 0.00
***** Electrical Downtilt of the antenna [Degrees] ****************************
ANTENNA N ELECTRICAL_DOWNTILT 0.00
***** Transmitted power *******************************************************
ANTENNA N POWER _ANT_POWER
***** Power unit **************************************************************
ANTENNA N UNIT DBM
***** Repeater mode ***********************************************************
ANTENNA N REPEATER_MODE 0
***** Transparent Repeater amplification gain *********************************
ANTENNA N REPEATER_SIGNAL_AMPLIFICATION 20.00
***** Antenna Aperture ********************************************************
ANTENNA N APERTURE_AUTO 1
ANTENNA N ANTENNA_APERTURE 360.000
***** Individual threshold for received power related to cell selection *******
ANTENNA N INDIVIDUAL_THRESHOLD_CELL_SELECT 0
***** Threshold for received power related to cell selection ******************
ANTENNA N THRESHOLD_CELL_SELECT -100.000
***** Individual threshold for max. radius related to cell selection **********
ANTENNA N INDIVIDUAL_MAX_RADIUS 0
***** Threshold for max. radius related to cell selection *********************
ANTENNA N APERTURE_MAX_RADIUS 1000.000
***** Power as output power (0), EIRP (1), ERP (2) *************************
ANTENNA N POWER_MODE 0
***** Type of power amplifier (0 = Single Carrier, 1 = Multi Carrier) *********
ANTENNA N POWER_AMPLIFIER 0
***** Transmitted frequency [MHz] *********************************************
ANTENNA N FREQUENCY 3800.00
***** Polarization ************************************************************
ANTENNA N POLARIZATION 0
ANTENNA N POLARIZATION_ANGLE 0.000
ANTENNA N POLARIZATION_XPD 10.000
ANTENNA N OPTIMIZER_TRX_PARA_MODE 0
ANTENNA N OPTIMIZER_TRX_PARA_ORIENTATION 0.0 0.0 0.0 0.0 0.0 0.0
***** TRX Properties of Transmitter *******************************************
ANTENNA N TRX_PROPERTIES_INDIVIDUAL n
ANTENNA N NOISEFIGURE 5.00000
ANTENNA N KEYWORD_DIVERSITY_GAIN 0.00000
***** Properties of Feeding Cable *********************************************
* Feeding cable mode 0 = manual definition, 1 = geometrically defined  ******
ANTENNA N ANTENNA_CABLE_LOSS_MODE 0
ANTENNA N ANTENNA_CABLE_LOSS 0.00000
***** Properties for (dynamic) WCDMA system simulator *************************
ANTENNA N MAX_USER 1000
ANTENNA N MAX_DATA_RATE 20000.00000
ANTENNA N TDD_RATIO_INDIVIDUAL 0
ANTENNA N OFDM_GUARD_INTERVAL_INDIVIDUAL 0

ANTENNA N COMMON_CHANNEL_POWER 30.000
ANTENNA N TRX_SIGNAL_DELAY 0.00000
***** Prediction area for this transmitter ************************************
*** (Default or individual) ***************************************************
ANTENNA N PREDICTION_AREA_INDIVIDUAL n
***** Prediction area for this transmitter ************************************
*** (Default or individual) ***************************************************
ANTENNA N PREDICTION_AREA_INDIVIDUAL n

