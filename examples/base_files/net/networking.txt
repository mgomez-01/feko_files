**********************************************************************************
****** OUTPUT OF NETWORK PLANNING  ***********************************************
**********************************************************************************
* Basic name of all propagation output file **************************************
OUTPUT_PROPAGATION_FILES "PropName"
OUTPUT_PROPAGATION_MOBILE_STATION_FILES "MS Results"
* Basic name of all Propagation projects *****************************************
PROJECT_FILE "Y:\feko_files\projects\testwo\testwo"
* Separate propagation results in individual directories (for each TRX) **********
SEPARATION_DIRECTORY y
* Show results in a seperate window **********************************************
SHOW_RESULTS_IN_SEPARATE_WINDOW n
* Simulate each carrier individually *********************************************
SIMULATE_EACH_CARRIER_INDIVIDUALLY n
COMPUTATION_REQUIRED y
COMPUTATION_REQUIRED_EMC y
* Write results (power matrices) to ASCII file which can be read with MS EXCEL ***
EXCEL_OUTPUT n
* Write results of propagation modules to ASCII files ****************************
ASCII_OUTPUT_PROP y

