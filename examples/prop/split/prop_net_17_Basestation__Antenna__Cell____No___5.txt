*******************************************************************************
***** Basestation / Antenna / Cell    No   5 **********************************
*******************************************************************************
***** Antenna modified since last path loss computation ***********************
ANTENNA 5 MODIFIED n n
***** Antenna enabled *********************************************************
ANTENNA 5 DISABLED n
***** Type of antenna (single antenna, radiating cable, repeater, etc.) *******
* Single Antenna = 0, Radiating Cable = 3, Repeater = 4,... 
ANTENNA 5 ANTENNA_TYPE 0
***** Coordinates of the antenna      [x,y,z] *********************************
ANTENNA 5 POSITION 429070.2700000000, 4513399.2699999996, 8.8000000000 
***** Name (ID) of the antenna / cell *****************************************
ANTENNA 5 NAME "USTAR-DD1"
***** Number of Site to which antenna belongs (-1 if no site) *****************
ANTENNA 5 SITE_NUMBER 5
***** Antenna type (ISO, SECTOR) *********************************************
ANTENNA 5 TYPE SECTOR
***** Antenna pattern (Filename without extension) ****************************
ANTENNA 5 PATTERN_VERTICAL "Y:\feko_files\projects\testwo\antennas\VVSSP\VVSSP-360S-F_SBandCombined_00DT_3550"
ANTENNA 5 PATTERN_TYPE 4
***** Gain of Antenna *********************************************************
ANTENNA 5 ANTENNA_GAIN_VERTICAL 4.758
***** Horizontal Orientation of the antenna [Degrees] *************************
ANTENNA 5 HORIZONTAL 0.00 N
***** Vertical Orientation of the antenna (Downtilt) [Degrees] ****************
ANTENNA 5 VERTICAL 0.00
***** Electrical Downtilt of the antenna [Degrees] ****************************
ANTENNA 5 ELECTRICAL_DOWNTILT 0.00
***** Transmitted power *******************************************************
ANTENNA 5 POWER 36.00000
***** Power unit **************************************************************
ANTENNA 5 UNIT DBM
***** Repeater mode ***********************************************************
ANTENNA 5 REPEATER_MODE 0
***** Transparent Repeater amplification gain *********************************
ANTENNA 5 REPEATER_SIGNAL_AMPLIFICATION 20.00
***** Antenna Aperture ********************************************************
ANTENNA 5 APERTURE_AUTO 1
ANTENNA 5 ANTENNA_APERTURE 360.000
***** Individual threshold for received power related to cell selection *******
ANTENNA 5 INDIVIDUAL_THRESHOLD_CELL_SELECT 0
***** Threshold for received power related to cell selection ******************
ANTENNA 5 THRESHOLD_CELL_SELECT -100.000
***** Individual threshold for max. radius related to cell selection **********
ANTENNA 5 INDIVIDUAL_MAX_RADIUS 0
***** Threshold for max. radius related to cell selection *********************
ANTENNA 5 APERTURE_MAX_RADIUS 1000.000
***** Power as output power (0), EIRP (1), ERP (2) *************************
ANTENNA 5 POWER_MODE 0
***** Type of power amplifier (0 = Single Carrier, 1 = Multi Carrier) *********
ANTENNA 5 POWER_AMPLIFIER 0
***** Transmitted frequency [MHz] *********************************************
ANTENNA 5 FREQUENCY 3550.00
***** Polarization ************************************************************
ANTENNA 5 POLARIZATION 0
ANTENNA 5 POLARIZATION_ANGLE 0.000
ANTENNA 5 POLARIZATION_XPD 10.000
ANTENNA 5 OPTIMIZER_TRX_PARA_MODE 0
ANTENNA 5 OPTIMIZER_TRX_PARA_ORIENTATION 0.0 0.0 0.0 0.0 0.0 0.0
***** TRX Properties of Transmitter *******************************************
ANTENNA 5 TRX_PROPERTIES_INDIVIDUAL n
ANTENNA 5 NOISEFIGURE 5.00000
ANTENNA 5 KEYWORD_DIVERSITY_GAIN 0.00000
***** Properties of Feeding Cable *********************************************
* Feeding cable mode 0 = manual definition, 1 = geometrically defined  ******
ANTENNA 5 ANTENNA_CABLE_LOSS_MODE 0
ANTENNA 5 ANTENNA_CABLE_LOSS 0.00000
***** Properties for (dynamic) WCDMA system simulator *************************
ANTENNA 5 MAX_USER 1000
ANTENNA 5 MAX_DATA_RATE 20000.00000
ANTENNA 5 TDD_RATIO_INDIVIDUAL 0
ANTENNA 5 OFDM_GUARD_INTERVAL_INDIVIDUAL 0

ANTENNA 5 COMMON_CHANNEL_POWER 30.000
ANTENNA 5 TRX_SIGNAL_DELAY 0.00000
***** Prediction area for this transmitter ************************************
*** (Default or individual) ***************************************************
ANTENNA 5 PREDICTION_AREA_INDIVIDUAL n

