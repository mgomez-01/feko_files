*******************************************************************************
****** DEFINITION OF NEW TRX **************************************************
*******************************************************************************
***** Antenna modified since last path loss computation ***********************
ANTENNA 0 MODIFIED y y
***** Antenna enabled *********************************************************
ANTENNA 0 DISABLED n
***** Type of antenna (single antenna, radiating cable, repeater, etc.) *******
* Single Antenna = 0, Radiating Cable = 3, Repeater = 4,... 
ANTENNA 0 ANTENNA_TYPE 0
***** Coordinates of the antenna      [x,y,z] *********************************
ANTENNA 0 POSITION 0.0000000000, 0.0000000000, 15.0000000000 
***** Name (ID) of the antenna / cell *****************************************
ANTENNA 0 NAME "Antenna"
***** Number of Site to which antenna belongs (-1 if no site) *****************
ANTENNA 0 SITE_NUMBER -1
***** Antenna type (ISO, SECTOR) *********************************************
ANTENNA 0 TYPE ISO
***** Transmitted power *******************************************************
ANTENNA 0 POWER 10.00000
***** Power unit **************************************************************
ANTENNA 0 UNIT WATT
***** Repeater mode ***********************************************************
ANTENNA 0 REPEATER_MODE 0
***** Transparent Repeater amplification gain *********************************
ANTENNA 0 REPEATER_SIGNAL_AMPLIFICATION 20.00
***** Antenna Aperture ********************************************************
ANTENNA 0 APERTURE_AUTO 1
ANTENNA 0 ANTENNA_APERTURE 360.000
***** Individual threshold for received power related to cell selection *******
ANTENNA 0 INDIVIDUAL_THRESHOLD_CELL_SELECT 0
***** Threshold for received power related to cell selection ******************
ANTENNA 0 THRESHOLD_CELL_SELECT -100.000
***** Individual threshold for max. radius related to cell selection **********
ANTENNA 0 INDIVIDUAL_MAX_RADIUS 0
***** Threshold for max. radius related to cell selection *********************
ANTENNA 0 APERTURE_MAX_RADIUS 1000.000
***** Power as output power (0), EIRP (1), ERP (2) *************************
ANTENNA 0 POWER_MODE 0
***** Type of power amplifier (0 = Single Carrier, 1 = Multi Carrier) *********
ANTENNA 0 POWER_AMPLIFIER 0
***** Transmitted frequency [MHz] *********************************************
ANTENNA 0 FREQUENCY 2000.00
***** Polarization ************************************************************
ANTENNA 0 POLARIZATION 0
ANTENNA 0 POLARIZATION_ANGLE 0.000
ANTENNA 0 POLARIZATION_XPD 10.000
ANTENNA 0 OPTIMIZER_TRX_PARA_MODE 0
ANTENNA 0 OPTIMIZER_TRX_PARA_ORIENTATION 0.0 0.0 0.0 0.0 0.0 0.0
***** TRX Properties of Transmitter *******************************************
ANTENNA 0 TRX_PROPERTIES_INDIVIDUAL n
ANTENNA 0 NOISEFIGURE 5.00000
ANTENNA 0 KEYWORD_DIVERSITY_GAIN 0.00000
***** Properties of Feeding Cable *********************************************
* Feeding cable mode 0 = manual definition, 1 = geometrically defined  ******
ANTENNA 0 ANTENNA_CABLE_LOSS_MODE 0
ANTENNA 0 ANTENNA_CABLE_LOSS 0.00000
***** Properties for (dynamic) WCDMA system simulator *************************
ANTENNA 0 MAX_USER 1000
ANTENNA 0 MAX_DATA_RATE 20000.00000
ANTENNA 0 TDD_RATIO_INDIVIDUAL 0
ANTENNA 0 OFDM_GUARD_INTERVAL_INDIVIDUAL 0

ANTENNA 0 COMMON_CHANNEL_POWER 30.000
ANTENNA 0 TRX_SIGNAL_DELAY 0.00000
***** Prediction area for this transmitter ************************************
*** (Default or individual) ***************************************************
ANTENNA 0 PREDICTION_AREA_INDIVIDUAL n

ANTENNA 0 PREDICTION_AREA AREA_MODE 2
ANTENNA 0 PREDICTION_AREA COORDINATES_AREA 427915.8900000000 4511455.8070000000 430394.5220000000 4514546.3190000001
ANTENNA 0 PREDICTION_AREA RADIUS_AREA 200.0000000000


