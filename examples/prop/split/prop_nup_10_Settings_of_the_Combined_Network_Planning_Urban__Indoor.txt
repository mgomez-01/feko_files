*******************************************************************************
***** Settings of the Combined Network Planning Urban <=> Indoor **************
*******************************************************************************
***** Prediction with Combined Network Planning (CNP)  (y/n) ******************
CNP_INDOOR_PREDICTION n
KEYWORD_CNP_INDOOR_MATERIAL 1
KEYWORD_CNP_INDOOR_SUBDIVISION y
KEYWORD_CNP_INDOOR_MODEL 0
KEYWORD_CNP_INDOOR_OUTPUT 0
***** Prediction based on Fresnel coefficients (0) or empirical equations (1) *
CNP_INDOOR_DIFF_MODEL 1
***** Special parameters for Indoor IRT in CNP mode ***************************
CNP_INDOOR_IRT_PENETRATION_TX_BUILDING n
KEYWORD_CNP_INDOOR_IRT_INTERACTIONS_TOTAL 3
KEYWORD_CNP_INDOOR_IRT_INTERACTIONS_REFLECTION 2
KEYWORD_CNP_INDOOR_IRT_INTERACTIONS_DIFFRACTION 2
KEYWORD_CNP_INDOOR_IRT_INTERACTIONS_TRANSMISSION 3
***** Default material properties for indoor objects **************************
CNP_INDOOR_MATERIAL_TRANS 10.000
CNP_INDOOR_MATERIAL_REFL 9.000
CNP_INDOOR_MATERIAL_DIFF_IN_MIN 8.000
CNP_INDOOR_MATERIAL_DIFF_IN_MAX 15.000
CNP_INDOOR_MATERIAL_DIFF_OUT 5.000
CNP_INDOOR_MATERIAL_DIELECTRICITY 4.000
CNP_INDOOR_MATERIAL_PERMEABILITY 1.000
CNP_INDOOR_MATERIAL_CONDUCTIVITY 0.010
***** Cancel determination of further rays after reaching free space loss *****
FREE_SPACE_BREAK n

