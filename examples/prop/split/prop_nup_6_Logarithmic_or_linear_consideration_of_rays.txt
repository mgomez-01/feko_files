*******************************************************************************
***** Logarithmic or linear consideration of rays *****************************
*******************************************************************************
** (Based on power = 0, based on field strength = 1) **************************
RAYS_POWER_FIELD 0
** (Logarithmic values = 0, Linear values = 1) ********************************
RAYS_LOG_LIN 0
RAYS_MAX_PATH_LOSS 200.000
RAYS_MAX_DYNAMIC y 100.000
RAYS_MAX_NUMBER y 20

