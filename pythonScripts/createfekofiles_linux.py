#! /usr/bin/python3
import sys
import os
import pandas as pd
import pickle
import re
import questionary
from libs.customFormatter import CustomFormatter
import logging
import signal
import json
from rich import print
from pathlib import Path
import argparse
import shutil



logger = CustomFormatter.createLogger('Creating Feko Files:', './logs/createfekofiles_linux.log')

def custom_exit(status):
    logger.warning(f'exiting from status:{status}')
    sys.exit(status)


file_option = False


pick_path = os.path.abspath(os.path.join('..', 'pickled_data'))
proj_path = os.path.abspath(os.path.join('..', 'projects'))
routes_path = os.path.abspath(os.path.join('..', 'bus_routes'))
deployment_sites = pd.read_csv(os.path.join('..', 'powder_deployment_sites', 'powder-deploymentUTM.csv'))
antennas_path = os.path.join('..', 'antennas')
antenna_dir = os.path.abspath(antennas_path)
# antennas_misc_dir = os.path.join('misc', 'antennas')
antennas_misc_dir = 'antennas/misc'

# format of the name for antenna AW3828/924 take this form.
AW3828_path = 'antennas/AW3828/MSI/'  # .. with the filename below ending the path
AW3828_filenames = {'AW3828_3550_T0.Msi', 'AW3828_3550_T2.Msi', 'AW3828_3550_T5.Msi', 'AW3828_3550_T8.Msi', 'AW3828_3950_T10.Msi',
                    'AW3828_3950_T3.Msi', 'AW3828_3950_T6.Msi', 'AW3828_3950_T9.Msi', 'AW3828_3550_T10.Msi', 'AW3828_3550_T3.Msi',
                    'AW3828_3550_T6.Msi', 'AW3828_3550_T9.Msi', 'AW3828_3950_T1.Msi', 'AW3828_3950_T4.Msi', 'AW3828_3950_T7.Msi',
                    'AW3828_3550_T1.Msi', 'AW3828_3550_T4.Msi', 'AW3828_3550_T7.Msi', 'AW3828_3950_T0.Msi', 'AW3828_3950_T2.Msi',
                    'AW3828_3950_T5.Msi', 'AW3828_3950_T8.Msi'}

AW3924_path = 'antennas/AW3924/MSI/',
AW3924_filenames = {'AW3924_3300_T0.msi', 'AW3924_3500_T0.msi', 'AW3924_3700_T0.msi', 'AW3924_3900_T0.msi', 'AW3924_4100_T0.msi',
                    'AW3924_3350_T0.msi', 'AW3924_3550_T0.msi', 'AW3924_3750_T0.msi', 'AW3924_3950_T0.msi', 'AW3924_4150_T0.msi',
                    'AW3924_3400_T0.msi', 'AW3924_3600_T0.msi', 'AW3924_3800_T0.msi', 'AW3924_4000_T0.msi', 'AW3924_4200_T0.msi',
                    'AW3924_3450_T0.msi', 'AW3924_3650_T0.msi', 'AW3924_3850_T0.msi', 'AW3924_4050_T0.msi'}

VVSSP_path = 'antennas/msi-VVSSP-360S-F/',
VVSSP_filenames = {'VVSSP-360S-F_Port 10 -45_00DT_5150.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5900.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5850.msi', 'VVSSP-360S-F_VBandCombined_07DT_1850.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5175.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5925.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5875.msi', 'VVSSP-360S-F_VBandCombined_07DT_1865.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5200.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5150.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5900.msi', 'VVSSP-360S-F_VBandCombined_07DT_1880.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5225.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5175.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5925.msi', 'VVSSP-360S-F_VBandCombined_07DT_1900.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5250.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5200.msi', 'VVSSP-360S-F_SBandCombined_00DT_3400.msi', 'VVSSP-360S-F_VBandCombined_07DT_1915.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5275.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5225.msi', 'VVSSP-360S-F_SBandCombined_00DT_3425.msi', 'VVSSP-360S-F_VBandCombined_07DT_1920.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5300.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5250.msi', 'VVSSP-360S-F_SBandCombined_00DT_3450.msi', 'VVSSP-360S-F_VBandCombined_07DT_1930.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5325.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5275.msi', 'VVSSP-360S-F_SBandCombined_00DT_3475.msi', 'VVSSP-360S-F_VBandCombined_07DT_1950.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5350.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5300.msi', 'VVSSP-360S-F_SBandCombined_00DT_3500.msi', 'VVSSP-360S-F_VBandCombined_07DT_1962.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5375.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5325.msi', 'VVSSP-360S-F_SBandCombined_00DT_3525.msi', 'VVSSP-360S-F_VBandCombined_07DT_1990.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5400.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5350.msi', 'VVSSP-360S-F_SBandCombined_00DT_3550.apb', 'VVSSP-360S-F_VBandCombined_07DT_2020.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5425.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5375.msi', 'VVSSP-360S-F_SBandCombined_00DT_3550.msi', 'VVSSP-360S-F_VBandCombined_07DT_2100.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5450.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5400.msi', 'VVSSP-360S-F_SBandCombined_00DT_3575.msi', 'VVSSP-360S-F_VBandCombined_07DT_2110.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5475.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5425.msi', 'VVSSP-360S-F_SBandCombined_00DT_3600.msi', 'VVSSP-360S-F_VBandCombined_07DT_2132.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5500.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5450.msi', 'VVSSP-360S-F_SBandCombined_00DT_3625.msi', 'VVSSP-360S-F_VBandCombined_07DT_2155.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5525.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5475.msi', 'VVSSP-360S-F_SBandCombined_00DT_3650.msi', 'VVSSP-360S-F_VBandCombined_07DT_2170.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5550.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5500.msi', 'VVSSP-360S-F_SBandCombined_00DT_3675.ahb', 'VVSSP-360S-F_VBandCombined_07DT_2300.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5575.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5525.msi', 'VVSSP-360S-F_SBandCombined_00DT_3675.apb', 'VVSSP-360S-F_VBandCombined_07DT_2350.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5600.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5550.msi', 'VVSSP-360S-F_SBandCombined_00DT_3675.msi', 'VVSSP-360S-F_VBandCombined_07DT_2400.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5625.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5575.msi', 'VVSSP-360S-F_SBandCombined_00DT_3700.msi', 'VVSSP-360S-F_VBandCombined_07DT_2450.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5650.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5600.msi', 'VVSSP-360S-F_SBandCombined_00DT_3725.msi', 'VVSSP-360S-F_VBandCombined_07DT_2500.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5675.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5625.msi', 'VVSSP-360S-F_SBandCombined_00DT_3750.msi', 'VVSSP-360S-F_VBandCombined_07DT_2535.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5700.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5650.msi', 'VVSSP-360S-F_SBandCombined_00DT_3775.msi', 'VVSSP-360S-F_VBandCombined_07DT_2550.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5725.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5675.msi', 'VVSSP-360S-F_SBandCombined_00DT_3800.msi', 'VVSSP-360S-F_VBandCombined_07DT_2570.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5750.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5700.msi', 'VVSSP-360S-F_VBandCombined_07DT_1695.msi', 'VVSSP-360S-F_VBandCombined_07DT_2620.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5775.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5725.msi', 'VVSSP-360S-F_VBandCombined_07DT_1710.msi', 'VVSSP-360S-F_VBandCombined_07DT_2655.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5800.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5750.msi', 'VVSSP-360S-F_VBandCombined_07DT_1733.msi', 'VVSSP-360S-F_VBandCombined_07DT_2690.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5825.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5775.msi', 'VVSSP-360S-F_VBandCombined_07DT_1755.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5850.msi',
                   'VVSSP-360S-F_Port 9 +45_00DT_5800.msi', 'VVSSP-360S-F_VBandCombined_07DT_1795.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5875.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5825.msi',
                   'VVSSP-360S-F_VBandCombined_07DT_1800.msi'}

VVSSP_freq_filename_dict = {}
# {ant.split('_')[3].split('.')[0]:ant for ant in VVSSP_filenames}
for file in VVSSP_filenames:
    freq = file.split('_')[3].split('.')[0]
    if freq in VVSSP_freq_filename_dict.keys():
        VVSSP_freq_filename_dict[freq].append(file)
    else:
        VVSSP_freq_filename_dict[freq] = []
        VVSSP_freq_filename_dict[freq].append(file)

VVSSP_freq_filename_dict = {key: VVSSP_freq_filename_dict[key] for key in sorted(VVSSP_freq_filename_dict.keys())}

# creation of the freq to filename relation for adding in as a file
AW3828_freq_filename_dict = {ant.split('_')[1]: ant for ant in AW3828_filenames}
AW3924_freq_filename_dict = {ant.split('_')[1]: ant for ant in AW3924_filenames}


# set the path split var to keep this working in windows. would export malformed paths in windows. use createfekofiles.py in windows
if os.name == 'nt':
    main_dir = os.path.abspath('..').split('/')[-1]
else:
    main_dir = os.path.abspath('..').split('/')[-1]


def pull_paths_pkl(paths_file):
    if os.path.getsize(paths_file) == 0:
        logger.debug('file is empty')
        loaded_paths = []
    else:
        with open(paths_file, 'rb') as file:
            loaded_paths = pickle.load(file)
            logger.debug(f'loaded_paths:{loaded_paths}')
            loaded_paths = [str(Path(p)) for p in loaded_paths]
    logger.debug(f'loaded_paths after Path(p):{loaded_paths}')
    loaded_paths_joined = ''.join(loaded_paths)
    logger.debug(f"loaded_paths after join:{loaded_paths_joined}")
    loaded_paths_joined_split = loaded_paths_joined.split(';')
    logger.debug(f"loaded_paths after split:{loaded_paths_joined_split}")
    if loaded_paths_joined == '':
        confirm = False
    else:
        loaded_paths_joined_split = [path for path in loaded_paths_joined_split if path]
        logger.debug(f"loaded_paths after removing empties:{loaded_paths_joined_split}")
        confirm = questionary.confirm('would you like to use a saved path?').ask()
    if confirm:
        win_path = ''
        while win_path == '':
            path_selection = select_items('select a single path from the saved ones below:', loaded_paths_joined_split)
            if len(path_selection) != 1:
                logger.debug('please select a single path for the location of the project')
                win_path = ''
            else:
                win_path = path_selection[0]
        logger.debug(win_path)
    else:
        win_path = ''

    return loaded_paths_joined_split, win_path


def get_stored_path():
    # Prompt for project name and validate according to criteria
    try:
        paths_file = os.path.join('..', 'pickled_data', 'paths.pkl')
        saved_paths, win_path = pull_paths_pkl(paths_file)
        logger.debug(f'win_path:{win_path}')
        logger.debug(f'saved_paths:{saved_paths}')
        if win_path == '':
            name = ''
            while not re.match('^[A-Z][:0-9a-zA-Z_/]+$', name):
                name = input('please provide the abs path where this project will be located\n')
                logger.debug(f'name:{name}')
                logger.debug(f'name:{name}')
                logger.debug(f'Path(name):{Path(name)}')
                saved_paths.append(''.join(name))
                logger.debug(f'saved_paths:{saved_paths}')
                confirmation = questionary.confirm('this cannot be validated by this script. please check the path and verify that it is correct before continuing\n').ask()
                if confirmation:
                    with open(paths_file, 'wb') as file:
                        saved_paths = ';'.join(saved_paths)
                        logger.debug(f'saved_paths:{saved_paths}')
                        pickle.dump(saved_paths, file)
                    logger.debug(f'win_path from name:{name}')
                    return name
                else:
                    name = ''
        else:
            logger.debug(f'win_path:{win_path}')
            return win_path
    except Exception as e:
        logger.error(f'problem with getting the path {e}')
        sys.exit(1)


# something I tried to hold all dir data, but working with it is a bit off from what I expect. will continue to attemp this style later on
def dir_tree_to_dict(start_path):
    tree_dict = {}
    for root, dirs, files in os.walk(start_path):
        tree_dict[f"..{root.split(main_dir)[1]}"] = {'dirs': dirs, 'files': files}
    return tree_dict


antennas_dict = dir_tree_to_dict(antenna_dir)


def handle_sigint(signum, frame):
    while True:
        try:
            selection = questionary.confirm('would you like to exit?').ask()
            if selection:
                logger.debug('user selected exit program. exiting')
                sys.exit(0)
            else:
                logger.debug('user selected no exit returning to execution.')
                break
        except Exception as e:
            logger.debug(f'error happened while handling sigint:\n{e}')


def get_project_name():
    # Prompt for project name and validate according to criteria
    name = ''
    while not re.match('^[0-9a-z_]+$', name):
        name = input('enter a name for the project.\nName must be lowercase, not contain symbols other than words, numbers, and be separated by underscores\n')
        logger.debug(f'name:{name}')
        projects = os.listdir(proj_path)
        if name in projects:
            logger.warning(f'project directory {name} already exists. choose a different name.')
            name = ''
    return name
    # Return the validated project name


def select_items(prompt, items):
    # if empty, return []
    if len(items) == 0:
        return []
    # Display the items and prompt the user to select by ticking boxes with an 'x'
    selection = questionary.checkbox(prompt, choices=items).ask()
    logger.debug(f'displaying res:{selection}')
    logger.debug('returning the selected items')
    return selection
    # Return the selected items


def create_project_directory(project_name):
    # Create a directory with the given project name
    try:
        logger.debug(f'creating project directory for {project_name} in {os.path.abspath(proj_path)}')
        os.mkdir(os.path.join(os.path.abspath(proj_path), project_name))
        logger.debug(f'directory {os.path.join(os.path.abspath(proj_path), project_name)} created successfully.')
        return os.path.join(os.path.abspath(proj_path), project_name)
    except FileExistsError:
        logger.debug('already exists. no need to create.')
        print('already exists. no need to create.')
        return os.path.join(os.path.abspath(proj_path), project_name)
    # Return the directory path after creating it


def write_csv_with_pandas(file_path, data):
    # Write the given data to a CSV file using Pandas
    try:
        data.to_csv(file_path, index=False)
        logger.debug(f'writing to csv at path:{file_path}')
    except Exception as e:
        logger.error(f'problem saving the data to path:{file_path}')
        logger.warning(f'data:\n{data}')
        logger.critical(f'error:\n{e}')
        sys.exit(1)


def create_symlink(target_file, link_name):
    # Create a symlink to the target file with the given link name
    try:
        os.link(target_file, link_name)
        logger.debug(f'creating sym link for data:{target_file} using name:{link_name}')
    except Exception as e:
        logger.debug(f'error:{e}')
        sys.exit(1)


def pickle_project_data(project_data, cache_file):
    # Serialize the project data using pickling
    # with open(cache_file, 'wb') as handle:
    #    pickle.dump(project_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    logger.debug(f'predone: pickled project:{cache_file} from:{project_data}')


def list_existing_projects(pickle_dir):
    # List all pickled projects in the specified directory
    projects = [f[:-4] for f in os.listdir(pickle_dir) if f.endswith('_proj.pkl')]
    return projects


def load_project_from_pickle(project_name, pickle_dir):
    # Load the project data from the pickled file
    # with open(os.path.join(pickle_dir, f"{project_name}.pkl"), 'rb') as handle:
    #     project_data = pickle.load(handle)
    # return project_data
    logger.debug(f'predone: loading project {project_name} from:{pickle_dir}')
    return []
    # returns empty data until implemented


def link_routes(routes, proj_name):
    if len(routes) == 0:
        return
    try:
        os.mkdir(os.path.join(proj_path, proj_name, 'routes'))
        for route in routes:
            os.link(os.path.join(routes_path, f'{route}.csv'), os.path.join(proj_path, proj_name, f'routes/{route}.csv'))
            logger.debug(f"\ntarget:{os.path.join(routes_path, f'{route}.csv')}\nlink:{os.path.join(proj_path, f'{route}.csv')}")
    except FileExistsError:
        logger.debug('already exists. no need to create.')
        print('already exists. no need to create.')



def link_antennas(antenna_list, proj_name):
    if len(antenna_list) == 0:
        return
    contains_misc = 0
    new_ant_dir = os.path.join(proj_path, proj_name, 'antennas')
    try:
        os.mkdir(new_ant_dir)
    except FileExistsError:
        logger.debug('already exists. no need to create.')
        print('already exists. no need to create.')
        
    logger.debug(f"successfully made new dir for antennas at {new_ant_dir}")
    logger.debug(f"new antenna dir path for proj at:{new_ant_dir}")
    for ant in antenna_list:
        if 'VVSSP' in ant:
            link_path = os.path.abspath(os.path.join(f'{antennas_path}', f'{ant}', 'msi'))
            target_path = os.path.join(new_ant_dir, ant)
        elif 'AW3828' in ant:
            print(os.path.abspath(os.path.join(antennas_path, ant, 'MSI')))
            try:
                os.mkdir(os.path.join(new_ant_dir, ant))
                link_path = os.path.abspath(os.path.join(antennas_path, ant, 'MSI'))
                target_path = os.path.join(new_ant_dir, ant, 'MSI')
            except FileExistsError:
                logger.debug('already exists. no need to create.')
                print('already exists. no need to create.')

        elif 'AW3924' in ant:
            print(os.path.abspath(os.path.join(antennas_path, ant, 'MSI')))
            link_path = os.path.abspath(os.path.join(antennas_path, ant, 'MSI'))
            target_path = os.path.join(new_ant_dir, ant)
        else:
            link_path = os.path.abspath(os.path.join(antennas_path, 'misc_antennas'))
            target_path = os.path.join(new_ant_dir, ant)
            contains_misc = 1
        print(f'linking {link_path} to {target_path}')
        try:
            os.symlink(link_path, target_path)
        except FileExistsError:
            logger.debug('already exists. no need to create.')
            print('already exists. no need to create symlink.')

    if contains_misc:
        # fix this later to only include the msi files in this dir
        try:
            link_path = os.path.abspath(f'{antenna_dir}/misc_antennas')
            target_path = os.path.join(new_ant_dir, 'misc')
            print(f'linking {link_path} to {target_path}')
            print(os.path.abspath(os.path.join(f'{antennas_path}', f'{ant}')))
            os.symlink(link_path, target_path)
        except FileExistsError:
            logger.debug('already exists. no need to create.')
            print('already exists. no need to create symlink.')
        except Exception as e:
            logger.debug(f'path for link:{antenna_dir}/misc_antennas already created.\n error:{e}')
    logger.debug('all antenna directories have been linked successfully')


def get_routes():
    # lists route files in the routes path to pull names.
    route_files = os.listdir(routes_path)
    route_files = [file.replace('.csv', '') for file in route_files if 'bakup' not in file]
    return route_files


# retuns a new frame that contains only the sites the user selected in the prompt
def collect_sites(sites):
    matches = []
    for site in sites:
        splitup = site.split(':')
        site_name = splitup[0].strip()
        site_type = splitup[1].strip()
        matching = deployment_sites.loc[(deployment_sites['Station'] == site_name) & (deployment_sites['Type'] == site_type)]
        matches.extend(matching.index.tolist())
    if len(matches) > 0:
        logger.debug('have matches')
        return deployment_sites.loc[matches]
    else:
        logger.debug('no matches found')
        return []


# pulls the site names from the deployment csv and lists the sites in nice readable format
def get_sites():
    logger.debug(f'\n{deployment_sites}')
    # Determine the maximum length of the station name and format output
    max_length = max(len(kind) for kind in deployment_sites['Station'])
    sites = [f"{site:<{max_length+1}}:{kind}" for site, kind in zip(deployment_sites['Station'], deployment_sites['Type'])]
    logger.debug(sites)
    return sites


def get_antennas():
    antennas = []
    for root in antennas_dict.keys():
        val = root.split('antennas')[1][1:].split('/')[0].replace('_', '')
        logger.debug(val)
        if 'msi-' in val:
            val = val.replace('msi-', '')
        elif 'antenna2d-' in val:
            val = val.replace('antenna2d-', '')
        elif 'misc' in val:
            logger.debug('\tmisc')
            logger.debug(antennas_dict[root]['files'])
        antennas.append(val)
    antennas = set(antennas)
    antennas.remove('')
    logger.debug(antennas)

    return antennas


def get_misc_antennas(selected_antennas):
    misc_antennas = []
    if 'misc' in selected_antennas:
        for element in antennas_dict['../antennas/misc_antennas']['files']:
            if 'msi' in element:
                misc_antennas.append(element.split('.')[0])
                logger.debug(misc_antennas)
        for val in selected_antennas:
            misc_antennas.append(val)
        logger.debug(f'misc_antennas:{misc_antennas}')
        misc_antennas.remove('misc')
        logger.debug(f'misc_antennas after misc removal:{misc_antennas}')
    return misc_antennas


global frequencies_so_far
frequencies_so_far = {}
global map_path
map_path = ''


def get_antenna_path_with_freq(stored_path, project_name, antenna):
    global frequencies_so_far
    logger.debug(f"antenna:{antenna}")
    confirmation = False
    if len(frequencies_so_far[antenna]) != 0:
        confirmation = questionary.confirm('reuse selected frequency?').ask()
    try:
        if antenna == 'AW3828':
            # AW3828 antenna setup
            logger.debug('AW3828 antenna recognized')
            if not confirmation:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', AW3828_freq_filename_dict.keys())
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        if (selected_freq[0] not in frequencies_so_far[antenna]):
                            frequencies_so_far[antenna].append(selected_freq[0])
                        break
            else:
                while True:
                    selected_freq = select_items('select one of the previously selected frequencies', frequencies_so_far[antenna])
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        break
            print(f'selected_freq:{selected_freq}')
            this_antenna_path = f"{stored_path}{project_name}/antennas/AW3828/{AW3828_freq_filename_dict[selected_freq[0]]}"
            logger.debug(f'this antenna path = {this_antenna_path}')
            return this_antenna_path, selected_freq[0]

        elif antenna == 'AW3924':
            # AW3924 antenna setup
            logger.debug('AW3924 antenna recognized')
            while True:
                selected_port = select_items('Antenna has 4 ports, which one is of interest? select one', ['P1', 'P2', 'P3', 'P4'])
                if len(selected_port) != 1:
                    print('please select a single port since only one path can be selected for this antenna entry.')
                else:
                    break
            if not confirmation:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', AW3924_freq_filename_dict.keys())
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        if (selected_freq[0] not in frequencies_so_far[antenna]):
                            frequencies_so_far[antenna].append(selected_freq[0])
                        break
            else:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', frequencies_so_far[antenna])
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        break

            print(f'selected_freq:{selected_freq}')
            this_antenna_path = f"{stored_path}{project_name}/antennas/AW3924/{AW3924_freq_filename_dict[selected_freq[0]]}"
            logger.debug(f'this antenna path = {this_antenna_path}')
            return this_antenna_path, selected_freq[0]

        elif antenna == 'VVSSP':
            # VVSSP-360S-F antenna setup
            logger.debug('VVSSP-360S-F antenna recognized')
            if not confirmation:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', VVSSP_freq_filename_dict.keys())
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        if (selected_freq[0] not in frequencies_so_far[antenna]):
                            frequencies_so_far[antenna].append(selected_freq[0])
                        break
            else:
                while True:
                    selected_freq = select_items('select one of the previously selected frequencies', frequencies_so_far[antenna])
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        break
            print(f'selected_freq:{selected_freq}')
            this_antenna_path = os.path.join(f"{stored_path}{project_name}", "/antennas/VVSSP/", f"{VVSSP_freq_filename_dict[selected_freq[0]][0].split('.')[0]}.msi")
            print(f'this antenna path = {this_antenna_path}')
            return this_antenna_path, selected_freq[0]

        else:
            # some strategy for selecting the correct msi file for the misc antenna is needed. currently adds straightaway
            this_antenna_path = f"{stored_path}{project_name}/{antennas_misc_dir}/{antenna}.msi"
            print(f'this_antenna_path:{this_antenna_path}')
            return this_antenna_path, selected_freq[0]
    except Exception as e:
        logger.error(f'problem in get_antenna_path_with_freq({stored_path},{project_name},{antenna}):{e}')
        custom_exit(1)


def finalize_site_frame(frame: pd.DataFrame, sites, antennas, project_name) -> pd.DataFrame:
    global frequencies_so_far
    if frame.empty:
        logger.debug(f'frame is empty {frame}')
        return
    if len(sites) == 0:
        logger.debug(f'no sites provided {sites}')
        return
    if len(sites) == 0:
        logger.debug(f'no antennas provided {antennas}')
        return

    logger.debug(f'\nframe:{frame}')
    logger.debug(f'\nsites:{sites}')
    logger.debug(f'\nantennas:{antennas}')
    frame_copy = pd.DataFrame(frame).assign(frequency=0, power=0, az=0, el=0, sector=0, antpath='')
    stored_path = get_stored_path()
    print(f'stored_path:{stored_path}')
    site2sectors = {}
    for station in frame['Station']:
        sectors = -1
        while sectors < 0:
            try:
                sectors = input(f'how many sectors will {station} have?')
                sectors_int = int(sectors)
                logger.debug(f'sectors correct sectors:{sectors_int}')
                sectors = sectors_int
            except ValueError:
                logger.debug(f'sectors must be an integer. entered:{sectors}')
                sectors = -1
                continue
            if sectors_int > 4:
                logger.debug('max sectors is 4 for now. setting to 4')
                sectors = 4
                sectors_int = sectors
                site2sectors[station] = sectors_int
                break
            elif sectors_int < 1:
                logger.debug('sectors must be greater than 0 and less than 5')
                sectors = -1
            else:
                site2sectors[station] = sectors_int
        logger.debug(f'site:{station}\nsite2sectors:{site2sectors}')

    for site in site2sectors.keys():
        logger.debug(f'range(site2sectors[{site}]):{list(range(site2sectors[site]))}')
        for i in range(site2sectors[site]):
            while True:
                antenna = select_items(f'chose an antenna for the {site} site', antennas)
                if len(antenna) != 1:
                    logger.debug('please select one antenna')
                else:
                    break
            logger.debug(f'adding in sector {i} with antenna {antenna[0]} to {site}')
            logger.debug(f"{stored_path}{antenna[0]}")
            row_to_duplicate = frame_copy[(frame_copy['Station'] == site)]
            logger.debug(f"first copy:\n{row_to_duplicate}")
            row_to_duplicate = row_to_duplicate[row_to_duplicate['sector'] == 0].copy()
            logger.debug(f"copy of copy made:\n{row_to_duplicate}")
            row_to_duplicate['sector'] = i + 1
            row_to_duplicate['Station'] = f"{site}{i + 1}"
            logger.debug(f"sector added:\n{row_to_duplicate}")
            logger.debug(f'site2sectors[site]:\n{site2sectors[site]}')
            row_to_duplicate['az'] = i * 360.0 / site2sectors[site]
            logger.debug(f"az added:\n{row_to_duplicate}")
            row_to_duplicate['el'] = 0
            logger.debug(f"el added:\n{row_to_duplicate}")
            # path needs to be Joined as the path for stored with project name added as well. also ask user about freq for the site
            selected_port_path, freq = get_antenna_path_with_freq(stored_path, project_name, antenna[0])
            logger.debug(f'new path = {selected_port_path}')
            row_to_duplicate['antpath'] = selected_port_path
            row_to_duplicate['frequency'] = freq
            row_to_duplicate['power'] = 36  # add in something to get the power from the antenna specs. hardcode for now
            logger.debug(f"ant path added:\n{row_to_duplicate}")
            frame_copy = pd.concat([frame_copy, row_to_duplicate]).reset_index(drop=True)
    rows_to_drop = frame_copy[frame_copy['sector'] == 0].index
    frame_copy = frame_copy.drop(rows_to_drop)
    logger.debug(f"final frame dropped duplicates and all info added except elevation and power:\n{frame_copy}")
    # next is the setup of the radio info. associate the radio to a power level and set power column
    logger.debug(f'\nframe sent back\n:{frame_copy}')
    logger.debug(f'stored path provided:{stored_path}')
    # change the name of all columns to match feko expectations
    # Rename columns
    frame_copy.rename(columns={
        'sector': 'Number',
        'az': 'Azimuth',
        'el': 'Downtilt',
        'Station': 'Name',
        'UTM_Easting': 'Longitude',
        'UTM_Northing': 'Latitude',
        'antpath': 'AntennaPattern',
        'Height (m)': 'Height'
    }, inplace=True)
    frame_copy.drop(['Latitude (deg N)', 'Longitude (deg E)'], axis=1, inplace=True)  # This will drop columns with names 'Col1' and 'Col2'.
    logger.debug(f"final frame:\n{frame_copy}")
    return frame_copy



def fill_template_file(section, file_path, config_dict, text_updater):
    try:
        with open(file_path, 'r') as f:
            text = f.read()
            logger.debug(f"Text prior to replacement in section {section}: {text}")

            for key, value in config_dict.items():
                text = text_updater(text, key, value)

            logger.debug(f"Text after replacement in section {section}: {text}")
        return text
    except Exception as e:
        logger.error(f"Error in fill_template_file for section {section}: {e}")
        custom_exit(1)



# all base files located in these places below. these are taken by the functions below and have their configs appended according to the choices
# in the script
## TODO: Restructure this to use the newer files in examples/prop/ instead.
## TODO: Must redo this parsing and file pulling. The order is apparently critical and Altair does not guarantee the sims will work if they are not in the expected order. see this convo for more: https://community.altair.com/community?id=community_question&sys_id=84fd0752db52fd50cfd5f6a4e29619b9

# one set of files will be used for prop, and another for net. only the net and nup will be needed for the most part since we only really care for the urban sims


def_coord_file_path = "./../examples/prop/split/prop_net_1_DEFINITION_OF_COORDINATE_SYSTEM.txt"
def_networking_file_path = "./../examples/base_files/net/networking.txt"
def_sim_area_file_path = "./../examples/base_files/net/sim_area.txt"
def_ms_ant_file_path = "./../examples/base_files/net/ms_ant.txt"

def_out_net_settings_file_path = "./../examples/base_files/net/output_network_settings.txt"
def_out_carrier_file_path = "./../examples/base_files/net/carriers.txt"
def_out_air_interface_path = "./../examples/base_files/net/air_interface.txt"
def_out_settings_file_path = "./../examples/base_files/net/output_settings.txt"

def_clutter_file_path = "./../examples/base_files/net/clutter_traffic.txt"
def_site_file_path = "./../examples/base_files/net/site.txt"
def_defaults_file_path = "./../examples/base_files/net/default_antennas.txt"
def_antenna_file_path = "./../examples/base_files/net/antenna.txt"

def_mic_file_path = "./../examples/base_files/mic/Campus_clean_slate.txt"
def_nup_file_path = "./../examples/base_files/nup/Campus_clean_slate.txt"
def_wpi_file_path = "./../examples/base_files/wpi/Campus_clean_slate.txt"

global def_config_file_path
global config_net

def_config_file_path = "./../examples/base_files/config_test.json"

# still need to be replaced with the correct ones from the base_files dir
with open(def_config_file_path, 'r') as f:
    text = f.read()

config_net = json.loads(text)


# this width is just to keep the files looking like those made by feko
def format_text_with_padding(line, text, width=82):
    new_text = '*' * width + '\n'
    padded_line = f"{line:*<{width}}\n"
    new_text += padded_line + text
    return new_text

# adds in the coordinates info for feko. read in from config and populate the item
def populate_coords():
    global config_net
    try:
        with open(def_coord_file_path, 'r') as f:
            text = f.read()
            logger.debug(f"text prior to replacement:{text}")

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_coords ", text)
    except Exception as e:
        logger.error(f"error in populate_coords: {e}")
        custom_exit(1)


# read in from config and populate the item. TODO: finish implementing all populate functions and test full build and run
def populate_networking(project_name_path):
    global config_net
    try:
        with open(def_networking_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)
            config_net['PROJECT_FILE'] = f"\"{project_name_path}\""

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_networking ", text)
    except Exception as e:
        logger.error(f"error in populate_networking: {e}")


def populate_sim_area():
    global config_net
    try:
        with open(def_sim_area_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)
            
            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_sim_area ", text)
    except Exception as e:
        logger.error(f"error in populate_networking: {e}")

# default setup in ms_ant_path is for omni antenna
def populate_ms_ant():
    global config_net
    try:
        with open(def_ms_ant_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)
            
            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_ms_ant ", text)
    except Exception as e:
        logger.error(f"error in populate_ms_ant: {e}")


def populate_output_network_settings():
    global config_net
    try:
        with open(def_out_net_settings_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_output_network_settings ", text)
    except Exception as e:
        logger.error(f"error in populate_output_network_settings: {e}")


def populate_carriers():
    global config_net
    try:
        with open(def_out_carrier_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_output_carriers ", text)
    except Exception as e:
        logger.error(f"error in populate_carriers: {e}")


def populate_output_air_interface():
    global config_net
    try:
        with open(def_out_air_interface_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_output_air_interface ", text)
    except Exception as e:
        logger.error(f"error in populate_output_air_interface: {e}")



def populate_output_settings():
    global config_net
    try:
        with open(def_out_settings_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_output_settings ", text)
    except Exception as e:
        logger.error(f"error in populate_output_settings: {e}")



def populate_clutter(map_path):
    global config_net
    try:
        with open(def_clutter_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)
            config_net['CLUTTER_DATABASE_TRAFFIC'] = map_path

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
            logger.debug(f"text after replacement:{text}")
        return format_text_with_padding("* in populate_clutter ", text)
    except Exception as e:
        logger.error(f"error in populate_clutter: {e}")


def populate_sites(sites_frame):
    global config_net
    try:
        with open(def_site_file_path, 'r') as f:
            template_text = f.read()
            logger.debug(template_text)

            def update_text_from_row(row_number, row):
                text = template_text
                
                # Replace 'SITE N' with the actual row number
                text = re.sub(r'SITE N', f'SITE {row_number}', text)
                
                # Replace 'SITE_NAME "Example_Name"' with the actual site name
                site_name = row['Name']
                text = re.sub(r'SITE_NAME "Example_Name"', f'SITE_NAME "{site_name}"', text)
                
                # Replace 'SITE_LOCATION Lon Lat height' with the actual values
                lon = row['Longitude']
                lat = row['Latitude']
                height = row['Height']
                text = re.sub(r'SITE_LOCATION Lon Lat height', f'SITE_LOCATION {lon} {lat} {height}', text)
                # Replace other placeholders
                for key, value in row.items():
                    text = re.sub(rf"{key} .*", f"{key} {value}", text)
                
                return text

            # Use pandas apply to generate a series of text blocks
            text_blocks = sites_frame.apply(lambda row: update_text_from_row(row.name + 1, row), axis=1)
            # Concatenate all text blocks into one large text block
            final_text = '\n'.join(text_blocks)
            
        return format_text_with_padding("* in populate_sites ", final_text)
        
    except Exception as e:
        logger.debug(f"error in populate_sites: {e}")


def populate_antennas(sites_frame):
    global config_net
    try:
        ant_num = sites_frame.shape[0]
        #print(f"ant_num: {ant_num}")
        config_net['COUNTER_SITE'] = ant_num
        config_net['COUNTER_TRX'] = ant_num
        
        with open(def_antenna_file_path, 'r') as f:
            template_text = f.read()
            logger.debug(template_text)
        
            def update_text_from_row(row_number, row):
                text = template_text
                # Replace 'ANTENNA N' with the actual row number
                text = re.sub(r'ANTENNA N', f'ANTENNA {row_number}', text)
                text = re.sub(r'No N', f'No {row_number}', text)
                # Define replacements
                lon, lat, height = row['Longitude'], row['Latitude'], row['Height']
                position_replacement = f'POSITION {lon} {lat} {height}'
                # Replace other placeholders
                replacements = {
                    'NAME "_ANT_NAME"': f'NAME "{row["Name"]}"',
                    'POWER _ANT_POWER': f'POWER {row["power"]}',
                    'FREQUENCY .*': f'FREQUENCY {row["frequency"]}',
                    'PATTERN_VERTICAL "_ANT_PATTERN_PATH"': f'PATTERN_VERTICAL "{row["AntennaPattern"]}"',
                    'POSITION Lon/utm_format Lat/utm_format': position_replacement
                }
                
                for key, value in replacements.items():
                    text = re.sub(rf'ANTENNA {row_number} {key}', f'ANTENNA {row_number} {value}', text)
                
                return text
            # Use pandas apply to generate a series of text blocks
            text_blocks = sites_frame.apply(lambda row: (print(f"Index: {row.name+1}, Data: {row}"), update_text_from_row(row.name + 1, row))[1], axis=1)
            
            # Concatenate all text blocks into one large text block
            final_text = '\n'.join(text_blocks)
            print(sites_frame)
            logger.debug(f"text after replacement: {final_text}")
        
        return format_text_with_padding("* in populate_antennas ", final_text)
        
    except Exception as e:
        logger.error(f"error in populate_antennas: {e}")

def populate_defaults():
    try:
        with open(def_defaults_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)
        return format_text_with_padding("* in populate_defaults ", text)
    except Exception as e:
        logger.error(f"error in populate_defaults: {e}")


def build_net(proj_name_path, map_path, sites_frame):
    global config_net
    new_text = ''
    print(f"config_net: {config_net}")
    # Reset the index of the DataFrame and drop the old index column
    sites_frame.reset_index(drop=True, inplace=True)
## TODO
    # functions needed for building the files for project
    # net file may be different given a sim being for prop only or net only, or both.
    # create switch for populate_functions in case of the flag passed in at runtime
    populate_functions = [
        populate_coords,
        lambda: populate_networking(proj_name_path),
        populate_sim_area,
        populate_ms_ant,
        lambda: populate_clutter(map_path),
        populate_defaults,
        lambda: populate_sites(sites_frame),
        lambda: populate_antennas(sites_frame),
        populate_output_network_settings,
        populate_carriers,
        populate_output_air_interface,
        populate_output_settings
    ]
    for func in populate_functions:
        new_text += func()
    return new_text


# needed for building the mimo config files for feko
def build_mic():
    global config_net
    try:
        with open(def_mic_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
        return format_text_with_padding("* in build_mic ", text)
    except Exception as e:
        logger.error(f"error in build_mic: {e}")
    


# not entirely sure about this one yet, still needs to be added for ... reasons
def build_nup():
    global config_net
    try:
        with open(def_nup_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
        return format_text_with_padding("* in build_nup ", text)
    except Exception as e:
        logger.error(f"error in build_nup: {e}")


def build_wpi():
    global config_net
    try:
        with open(def_wpi_file_path, 'r') as f:
            text = f.read()
            logger.debug(text)

            def text_updater(text, key, value):
                return re.sub(rf"{key} .*", f"{key} {value}", text)

            for key, value in config_net.items():
                text = text_updater(text, key, value)
        return format_text_with_padding("* in build_wpi ", text)
    except Exception as e:
        logger.error(f"error in build_wpi: {e}")




def main():
    global frequencies_so_far
    global map_path
    try:
        project_name = get_project_name()
        # obtain routes from csv and sites from csv to ask user
        route_set = get_routes()
        site_set = get_sites()
        antennas = get_antennas()
        selected_routes = select_items("Choose routes:", route_set)
        selected_sites = select_items("Choose sites:", site_set)
        selected_antennas = select_items("Choose antennas:", antennas)
        if 'misc' in selected_antennas:
            all_antennas = get_misc_antennas(selected_antennas)
            all_antennas = select_items("Misc antennas added. Select again:", all_antennas)
        else:
            all_antennas = selected_antennas

        for ant in all_antennas:
            frequencies_so_far[ant] = []
        logger.debug(f'antennas:{all_antennas}')
        map_choice = select_items("Choose map:", ["Premade Campus Map", "Other Custom Map"])
        logger.debug(f'map_choice:{map_choice}')
        if map_choice[0] == "Premade Campus Map":
            # create project directory in projects and place files in there
            project_dir = create_project_directory(project_name)
            new_siteframe = collect_sites(selected_sites)
            logger.debug(f'new_siteframe:\n{new_siteframe}')
            logger.debug(f'project_dir:{project_dir}')
            logger.debug(f'routes:{selected_routes}\nsites:{selected_sites}\nmap:{map_choice}')
            # create sym links for the routes selected and add to the project
            link_routes(selected_routes, project_name)
            # add antennas to site frame if chosen
            if len(all_antennas) != 0:
                logger.debug('antennas not empty')
                link_antennas(all_antennas, project_name)
            else:
                logger.debug('antennas looks empty')
                logger.debug(all_antennas)
            # map is always in this location. link to it and add to directory
            create_symlink("../mapdata/map_UofU_campus_outputfile.osm", os.path.join(project_dir, "UofU_osm_map.osm"))
        else:
            while True:
                map_path = input('please provide a path for the map you will use.\n')
                try:
                    create_symlink(map_path, os.path.join(project_dir, "Custom_map.osm"))
                    logger.debug(f'map path: {map_path}')
                    break
                except Exception as e:
                    logger.error(f'error occured in symlinking of custom map. check the path provided and try again\nexception:{e}')
        logger.debug('all files linked successfully. Creating sites csv')

        final_frame = finalize_site_frame(new_siteframe, selected_sites, all_antennas, project_name)
        write_csv_with_pandas(os.path.join(project_dir, "sites.csv"), final_frame)

        
        # copy files from the base files dir and build up the net, nup, wst, mic files for this project
        target_file_path = os.path.join(project_dir, f"{project_name}")
        logger.debug(f"writing net/mic/nup/wpi files in {target_file_path}")
        with open(f"{target_file_path}.net", 'w') as target_file:
            new_file_text = build_net(target_file_path, map_path, final_frame)
            target_file.write(new_file_text)
        with open(f"{target_file_path}.mic", 'w') as target_file:
            new_file_text = build_mic()
            target_file.write(new_file_text)
        with open(f"{target_file_path}.nup", 'w') as target_file:
            new_file_text = build_nup()
            target_file.write(new_file_text)
        with open(f"{target_file_path}.wpi", 'w') as target_file:
            new_file_text = build_wpi()
            target_file.write(new_file_text)
        
            
        # read in dir and go one by one on the dirs building up the files
        # create the file and write to proj_dir
        # test execution of feko winprop with the project built

    except Exception as e:
        logger.error(f'problem with creation process from:{e}')
        sys.exit(1)
    logger.info("Project setup complete!")


# Additions below are for the design of the project from a config file


def validate_and_get_project_name(config):
    project = config.get('project')
    project_name = project.get('name')
    if not re.match('[a-zA-Z0-9_]*', project_name):
        raise ValueError(f'Invalid project name: {project_name}')
    print(f"project_name: {project_name}")
    return project_name


def validate_and_get_map(config):
    project = config.get('project')
    premade = project.get('Premade Campus Map')
    if premade:
        logger.debug(f'Premade Campus Map: {premade}, default')
        map_choice = os.path.abspath(os.path.join("..", "examples", "UofU_osm_map.osm"))
    else:
        map_choice = project.get('map')
        logger.debug(f"Premade Campus Map: {premade}, using supplied map: {map_choice}")
        if not os.path.exists(f"{map_choice}.osm"):
            print(f'error: {map_choice} does not seem to exist, are you using an abs path for your map?')
            raise ValueError(f'Invalid map name: {map_choice} does not exist.')
    logger.debug(f'Map: {map_choice}.')
    print(f'Map: {map_choice}.')
    return map_choice


# Similar functions for routes, sites, antennas, sectors, frequencies
# handles no routes, single, or multiples and checks that each are in the known routes dir
def validate_and_get_routes(config):
    logger.debug("pulling route data:")
    project = config.get('project')
    routes = project.get('routes')
    names = get_routes()
    logger.debug(f"names: {names}")
    logger.debug(f"known route names: {names}")
    if routes == []:
        print("no routes provided")
        return {}
    for route in routes:
        matching_names = {name for name in names if route in name}
        if matching_names:
            logger.debug(f"route: {route} found in routes dir")
        else:
            raise ValueError(f"route: {route} not found amongst known routes. known routes located in ../bus_routes")
    logger.debug(f"routes: {routes}")
    print(f"routes: {routes}")
    return routes


# validators should check config and if problem arises, raise ValueError(f'Invalid option: {option}')
def validate_and_get_sites(config):
    logger.debug("pulling site data:")
    project = config.get('project')
    sites = project.get('sites')
    if sites == []:
        print("sites are needed for simulation. please fix the sites in the file. Sites names can be found in ../powder_deployment_sites/powder-deploymentUTM.csv in the Station column. Site name will be key to antenna_settings and sectors.")
        raise ValueError(f"need sites for simulation. sites: {sites}")
    names = [name for name in deployment_sites['Station']]
    logger.debug(f'names:{names}')
    for site in sites:
        matching_names = {name for name in names if site[0:-1] in name}
        if matching_names:
            logger.debug(f"site: {site[0:-1]} found in sites frame")
        else:
            raise ValueError(f"site: {site} not found amongst known sites. known sites located in ../powder_deployment_sites/powder-deploymentUTM.csv")
    print(f'sites: {sites}')
    return sites, names


def validate_and_get_antennas(config):
    logger.debug("pulling antenna data:")
    project = config.get('project')
    antennas = project.get('antennas')
    if antennas == []:
        print("antennas are needed for simulation. please fix the antennas in the file. Antenna names can be found in ../antennas/. Each antenna dir is a type of antenna.")
        raise ValueError(f"antennas: {antennas} cannot be empty.")
    ant_dirs = os.listdir(antenna_dir)
    logger.debug(f"antenna_dirs: {ant_dirs}")
    for antenna in antennas:
        matching_names = {name for name in ant_dirs if antenna in name}
        if matching_names:
            logger.debug(f"antenna: {antenna} found in antenna dir.")
        else:
            print(f"antenna: {antenna} not found in antenna dir. unable to proceed. Available antennas are:\n{ant_dirs}")
            raise ValueError(f"antenna {antenna} not in known antennas.")
    logger.debug(f"antennas: {antennas}")
    print(f"antennas: {antennas}")
    return antennas


def validate_and_get_sectors(config, names):
    logger.debug("pulling sector data:")
    sectors = config.get('sectors')
    for sector in sectors:
        if sectors[sector] < 1:
            raise ValueError(f"sectors have a min of 1. this sector is {sector}:{sectors[sector]} check config file for correct values")
        else:
            logger.debug(f"{sector}: {sectors[sector]}")
    for sector in sectors:
        matching_names = {name for name in names if sector[0:-1] in name}
        if matching_names:
            logger.debug(f"sector: {sector[0:-1]} found in site frame")
        else:
            raise ValueError(f"sector: {sector} not found amongst known sites. known sites located in ../powder_deployment_sectors/powder-deploymentUTM.csv")
    logger.debug(f"sectors: {sectors}")
    print(f"sectors: {sectors}")
    return sectors


def validate_and_get_antenna_settings(config, names, sectors, frame):
    a_set = config.get('antenna_settings')
    if a_set is None:
        print('antenna settings malformed or not included, check config antenna_settings section.')
        raise ValueError(f"antenna_settings: {a_set} not valid antenna settings")
    logger.debug(f"frame: {frame}")
    for site in sectors.keys():
        logger.debug(f"site: {site}")
        antenna = a_set[site]['antenna']
        power = a_set[site]['power']
        az = a_set[site]['az']
        freq = a_set[site]['frequency']
        path = a_set[site]['path']
        logger.debug(f"option:\t'antenna'  :\tvalue:\t{antenna}")
        logger.debug(f"option:\t'power'    :\tvalue:\t{power}")
        logger.debug(f"option:\t'az'       :\tvalue:\t{az}")
        logger.debug(f"option:\t'frequency':\tvalue:\t{freq}")
        logger.debug(f"option:\t'path'     :\tvalue:\t{path}\n")
        if freq not in path:
            print(f"freq: {freq} does not match freq for file path: {path}.")
            raise ValueError(f"sim freq: {freq} does not match filename {path}.")
        frame['AntennaPattern'] = path
        frame['power'] = power
        frame['frequency'] = freq
        frame['Azimuth'] = az
    print(f"sites frame:\n{frame}")
        # TODO: set up the site.csv file here since all info is contained here. the csv can be built here and we
        # could actually return the csv instead of just the settings ?  think on this
    logger.debug(f'antenna_settings: {a_set}')
    return a_set
        
# Uses the same kind of flow as the original but following the file options
def file_option_main(args):
    logger.debug('in file_option_main\n')
    global def_config_file_path
    global config_net
    # Read the JSON config file
    if len(args.file) == 1:
        config_path_builder = args.file[0]
    else:
        config_path_builder = args.file[0]
        def_config_file_path = args.file[1]
    with open(config_path_builder, 'r') as f:
        config_builder = json.load(f)
        logger.debug(config_path_builder)
    with open(def_config_file_path, 'r') as f:
        config_net = json.load(f)
        logger.debug(config_net)
        
    try:
        project_name = validate_and_get_project_name(config_builder)
        logger.debug(f"project_name: {project_name}")
        map_choice = validate_and_get_map(config_builder)
        logger.debug(f"map_choice: {map_choice}")
        routes = validate_and_get_routes(config_builder)
        logger.debug(f"routes: {routes}")
        sites, names = validate_and_get_sites(config_builder)
        frame_copy = pd.DataFrame(deployment_sites).assign(frequency=0, power=0, az=0, el=0, sector=0, antpath='')
        frame_copy.rename(columns={
            'sector': 'Number',
            'az': 'Azimuth',
            'el': 'Downtilt',
            'Station': 'Name',
            'UTM_Easting': 'Longitude',
            'UTM_Northing': 'Latitude',
            'antpath': 'AntennaPattern',
            'Height (m)': 'Height'
        }, inplace=True)
        frame_copy.drop(['Latitude (deg N)', 'Longitude (deg E)'], axis=1, inplace=True)
        out_frame = pd.DataFrame()
        for site in sites:
            site_name = re.sub(r'[0-9]*', '', site)
            #logger.debug(f"site in sites: {site}")
            row_to_duplicate = frame_copy[(frame_copy['Name'] == site_name)].copy()
            row_to_duplicate['Name'] = site
            out_frame = pd.concat([out_frame, row_to_duplicate]).reset_index(drop=True)
        logger.debug(f"dataframe for sites found:\n{out_frame}")
        antenna_types = validate_and_get_antennas(config_builder)
        logger.debug(f"antenna types: {antenna_types}")
        sectors = validate_and_get_sectors(config_builder, names)
        logger.debug(f"sectors: {sectors}")
        antenna_settings = validate_and_get_antenna_settings(config_builder, names, sectors, out_frame)
        logger.debug(f"antenna_settings: {antenna_settings}")
        # antenna settings could be the CSV so we can put the files together using those for itertools
        print('all settings parsed, populating the files')
        # call build_net and
        # now need to do the work of the original script without asking questions.
        proj_name_path = os.path.abspath(os.path.join('..', 'projects', project_name))
        print(f"proj_name_path abs: {proj_name_path}")
        project_dir = create_project_directory(project_name)
        link_antennas(antenna_types, project_name)
        link_routes(routes, project_name)
        write_csv_with_pandas(os.path.join(proj_name_path, "sites.csv"), out_frame)
        build_net(proj_name_path, map_choice, out_frame)
        
        target_file_path = os.path.join(project_dir, f"{project_name}")
        logger.debug(f"writing net/mic/nup/wpi files in {target_file_path}")
        with open(f"{target_file_path}.net", 'w') as target_file:
            new_file_text = build_net(target_file_path, map_path, out_frame)
            target_file.write(new_file_text)
        # with open(f"{target_file_path}.mic", 'w') as target_file:
        #     new_file_text = build_mic()
        #     target_file.write(new_file_text)
        with open(f"{target_file_path}.nup", 'w') as target_file:
            new_file_text = build_nup()
            target_file.write(new_file_text)
        # with open(f"{target_file_path}.wpi", 'w') as target_file:
        #     new_file_text = build_wpi()
        #     target_file.write(new_file_text)

        
    except ValueError as e:
        logger.warning(f'Validation error: {e}')
        custom_exit(1)

    # TODO: 
    # copy files from the base files dir and build up the net, nup, wst, mic files for this project
    # read in dir and go one by one on the dirs building up the files
    # create the file and write to proj_dir
    # test execution of feko winprop with the project built



if __name__ == "__main__":
    signal.signal(signal.SIGINT, handle_sigint)
    parser = argparse.ArgumentParser(description='script for building up a project for feko', formatter_class=argparse.RawTextHelpFormatter)
    # Add options
    parser.add_argument('-S', '--sim-type', type=str, help='Simulation [Type]:P|N|B -*-NOTE-*- not yet complete\nP:propagation\nN:networking\nB:Both\n')
    parser.add_argument('-D', '--debug', action='store_true', help='Enable debugging prints')
    parser.add_argument('-f', '--file', type=str, nargs='+', help='Specify a JSON filename - lots done but still not complete')
    args = parser.parse_args()
    if args.debug:
        CustomFormatter.setLogLevel(logger, logging.DEBUG)
    else:
        CustomFormatter.setLogLevel(logger, logging.INFO)

    logger.debug('in dundermain\n')
    logger.debug(f"args.file: {args.file}")
    if args.file:
        for arg in args.file:
            if not re.match(r'.*\.json$', arg):
                print(f'filename not provided correctly. {arg} is not a proper json filename. Please provide filename if using the -f flag\n')
                custom_exit(1)
            print(f"Filename is {arg}")
        file_option_main(args)
    else:
        logger.debug('no config file provided. moving to usual version of script.\n')
        main()
