#!/usr/bin/env python3


import logging

class CustomFormatter(logging.Formatter):

    """A custom Formatter class for logging."""

    COLOR_CODES = {
        'grey': "\x1b[38;5;228m",
        'green': "\x1b[38;5;2m",
        'yellow': "\x1b[33;20m",
        'red': "\x1b[31;5;20m",
        'bold_red': "\x1b[31;1m",
        'reset': "\x1b[0m",
    }

    LOG_FORMAT = "%(asctime)s - %(name)s - %(levelname)s - %(message)s (%(filename)s:%(lineno)d)"

    FORMATS = {
        logging.DEBUG: COLOR_CODES['grey'] + LOG_FORMAT + COLOR_CODES['reset'],
        logging.INFO: COLOR_CODES['green'] + LOG_FORMAT + COLOR_CODES['reset'],
        logging.WARNING: COLOR_CODES['yellow'] + LOG_FORMAT + COLOR_CODES['reset'],
        logging.ERROR: COLOR_CODES['red'] + LOG_FORMAT + COLOR_CODES['reset'],
        logging.CRITICAL: COLOR_CODES['bold_red'] + LOG_FORMAT + COLOR_CODES['reset'],
        'FILE': LOG_FORMAT,
    }


    def format(self, record):
        log_fmt = self.FORMATS.get(record.levelno)
        formatter = logging.Formatter(log_fmt)
        return formatter.format(record)

    def createLogger(purpose='default: Change this by providing arg to createLogger.', filename=None):
        logger = logging.getLogger(purpose)
        logger.setLevel(logging.DEBUG)
        ch = logging.StreamHandler()
        ch.setLevel(logging.DEBUG)
        ch.setFormatter(CustomFormatter())
        logger.addHandler(ch)
        # file handler
        if filename is not None:
            fh = logging.FileHandler(filename)
            fh.setLevel(logging.DEBUG)
            fh.setFormatter(CustomFormatter())
            logger.addHandler(fh)
        return logger

    def setLogLevel(logger, level):
        logger.setLevel(level)
        for handler in logger.handlers:
            handler.setLevel(level)



    def __init__(self):
        super().__init__()


def main():
    logger = CustomFormatter.createLogger()

    logger.info("info")
    logger.debug("debug")
    logger.warning("warn")
    logger.error("error")
    logger.critical("Crit")
    input('press enter to exit...')




if __name__ == "__main__":
    main()
