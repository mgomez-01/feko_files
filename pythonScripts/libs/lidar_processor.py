import laspy
import numpy as np
from scipy.spatial import cKDTree

class LidarProcessor:
    def __init__(self, las_file_path):
        # Read the LiDAR data
        self.las_data = laspy.read(las_file_path)
        # Extract the coordinates and heights
        self.lidar_coords = np.vstack((self.las_data.x, self.las_data.y)).T
        self.lidar_heights = self.las_data.z

        # Create a KD-Tree for the LiDAR data
        self.lidar_kd_tree = cKDTree(self.lidar_coords)

    def get_building_height(self, x, y, radius=10, k=5):
        # Query k nearest neighbors within the specified radius
        indices = self.lidar_kd_tree.query_ball_point((x, y), r=radius)

        # If no points are found within the radius, fall back to querying the k nearest points
        if not indices:
            _, indices = self.lidar_kd_tree.query((x, y), k=k)

        # Extract the z coordinates (heights) of the found points
        heights = self.lidar_heights[indices]

        # Return the maximum height
        return np.max(heights)


    def get_cloud(self):
        return self.las_data
