import os
import shutil

dirs = [directory for directory in os.listdir('.') if os.path.isdir(directory)]
dirs
target_filename = "Power.txt"  # Replace with your specific filename
root_directory = '.'  # Start from the current project directory, change if needed, but will look into the prop and ms results for Power.txt
newfiles_dir = 'powerfiles'

for dirpath, dirnames, filenames in os.walk(root_directory):
    for filename in filenames:
        if filename == target_filename:
            full_path = os.path.join(dirpath, filename)
            print(full_path)
            splitpath = full_path.split('.txt')[0].split('/')
            print(splitpath)
            print(f"new_filename: {splitpath[2]}_{splitpath[3]}.txt")
            print(f"copy: shutil.copy({full_path},{os.path.abspath(newfiles_dir)}/{splitpath[2]}_{splitpath[3]}.txt)")
            shutil.copy(full_path, f"{os.path.abspath(newfiles_dir)}/{splitpath[2]}_{splitpath[3]}.txt")
