#! /usr/bin/python3
import re
from rich import print
import argparse
import sys

def split_file_by_section(filename):
    with open(filename, 'r') as f:
        lines = f.readlines()

    sections = []
    current_section = []
    
    n = len(lines)
    for i in range(n):
        # Check for the repeating asterisks
        if lines[i].startswith("****"):
            # Check if an n+2 line exists and matches the current line
            if i + 2 < n and lines[i] == lines[i + 2]:
                # Save the current section and start a new one
                if current_section:
                    sections.append(current_section)
                current_section = []
                
                # Extract meaningful name for the new section from n+1 line
                section_name = re.sub(r'[^\w\s]', '', lines[i + 1]).strip().replace(' ', '_')
                current_section.append(f"Section: {section_name}\n")
        
        # Add the line to the current section
        current_section.append(lines[i])
    
    # Add the last section if it exists
    if current_section:
        sections.append(current_section)

    return sections

def save_indiv(sections, file_type, sim):
    # To save the sections into separate files
    names = []
    for i, section in enumerate(sections):
        section_name_line = section[0].replace("Section: ", "").strip()
        print(f"{sim}_{file_type}_{i}_{section_name_line}.txt")
        with open(f"../examples/{sim}/split/{sim}_{file_type}_{i}_{section_name_line}.txt", "w") as f:
            f.writelines(section[1:])
    return names
    


def main(file_name):
    print("here in main")
    sections = split_file_by_section(file_name)
    print(sections)
    save_indiv(sections, file_name.split('/')[-1].split('.')[1], "prop")


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Creator of the sys files by splitting into sections', formatter_class=argparse.RawTextHelpFormatter)
    # Add options
    parser.add_argument('-F', '--file', type=str, help='takes a file as input')
    args = parser.parse_args()
    if args.file:
        if not re.match(r'.*\.(net|mic|nup|wpi)$', args.file):
            print(f'filename not provided correctly. {args.file} is not a net, mic, wpi, or nup filename. Please provide one of those for splitting.\n')
            sys.exit(1)
        main(args.file)
    else:
        print('no config file provided. exiting.\n')
        sys.exit(1)
        
        # sections = split_file_by_section(filename)
