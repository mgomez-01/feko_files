#! /usr/bin/python3
import sys
import os
import pandas as pd
import pickle
import re
import questionary
from libs.customFormatter import CustomFormatter
import logging
import signal
import argparse
from rich import print
from pathlib import Path
import json
logger = CustomFormatter.createLogger('Creating Feko Files:', './logs/createfekofiles.log')


def custom_exit(status):
    logger.warning(f'exiting from status:{status}')
    sys.exit(status)


file_option = False

pick_path = os.path.abspath(os.path.join('..', 'pickled_data'))
proj_path = os.path.abspath(os.path.join('..', 'projects'))
routes_path = os.path.abspath(os.path.join('..', 'bus_routes'))
deployment_sites = pd.read_csv(os.path.join('..', 'powder_deployment_sites', 'powder-deploymentUTM.csv'))
antennas_path = os.path.join('..', 'antennas')
antenna_dir = os.path.abspath(antennas_path)
# antennas_misc_dir = os.path.join('misc', 'antennas')
# necessary to hardcode path for windows as below
antennas_misc_dir = 'antennas\\misc'

# format of the name for antenna AW3828/924 in windows take this form.
AW3828_path = 'antennas\\AW3828\\MSI\\'  # .. with the filename below ending the path
AW3828_filenames = {'AW3828_3550_T0.Msi', 'AW3828_3550_T2.Msi', 'AW3828_3550_T5.Msi', 'AW3828_3550_T8.Msi', 'AW3828_3950_T10.Msi',
                    'AW3828_3950_T3.Msi', 'AW3828_3950_T6.Msi', 'AW3828_3950_T9.Msi', 'AW3828_3550_T10.Msi', 'AW3828_3550_T3.Msi',
                    'AW3828_3550_T6.Msi', 'AW3828_3550_T9.Msi', 'AW3828_3950_T1.Msi', 'AW3828_3950_T4.Msi', 'AW3828_3950_T7.Msi',
                    'AW3828_3550_T1.Msi', 'AW3828_3550_T4.Msi', 'AW3828_3550_T7.Msi', 'AW3828_3950_T0.Msi', 'AW3828_3950_T2.Msi',
                    'AW3828_3950_T5.Msi', 'AW3828_3950_T8.Msi'}

AW3924_path = 'antennas\\AW3924\\MSI\\',
AW3924_filenames = {'AW3924_3300_T0.msi', 'AW3924_3500_T0.msi', 'AW3924_3700_T0.msi', 'AW3924_3900_T0.msi', 'AW3924_4100_T0.msi',
                    'AW3924_3350_T0.msi', 'AW3924_3550_T0.msi', 'AW3924_3750_T0.msi', 'AW3924_3950_T0.msi', 'AW3924_4150_T0.msi',
                    'AW3924_3400_T0.msi', 'AW3924_3600_T0.msi', 'AW3924_3800_T0.msi', 'AW3924_4000_T0.msi', 'AW3924_4200_T0.msi',
                    'AW3924_3450_T0.msi', 'AW3924_3650_T0.msi', 'AW3924_3850_T0.msi', 'AW3924_4050_T0.msi'}

VVSSP_path = 'antennas\\VVSSP\\',
VVSSP_filenames = {'VVSSP-360S-F_Port 10 -45_00DT_5150.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5900.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5850.msi', 'VVSSP-360S-F_VBandCombined_07DT_1850.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5175.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5925.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5875.msi', 'VVSSP-360S-F_VBandCombined_07DT_1865.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5200.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5150.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5900.msi', 'VVSSP-360S-F_VBandCombined_07DT_1880.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5225.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5175.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5925.msi', 'VVSSP-360S-F_VBandCombined_07DT_1900.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5250.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5200.msi', 'VVSSP-360S-F_SBandCombined_00DT_3400.msi', 'VVSSP-360S-F_VBandCombined_07DT_1915.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5275.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5225.msi', 'VVSSP-360S-F_SBandCombined_00DT_3425.msi', 'VVSSP-360S-F_VBandCombined_07DT_1920.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5300.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5250.msi', 'VVSSP-360S-F_SBandCombined_00DT_3450.msi', 'VVSSP-360S-F_VBandCombined_07DT_1930.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5325.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5275.msi', 'VVSSP-360S-F_SBandCombined_00DT_3475.msi', 'VVSSP-360S-F_VBandCombined_07DT_1950.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5350.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5300.msi', 'VVSSP-360S-F_SBandCombined_00DT_3500.msi', 'VVSSP-360S-F_VBandCombined_07DT_1962.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5375.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5325.msi', 'VVSSP-360S-F_SBandCombined_00DT_3525.msi', 'VVSSP-360S-F_VBandCombined_07DT_1990.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5400.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5350.msi', 'VVSSP-360S-F_SBandCombined_00DT_3550.apb', 'VVSSP-360S-F_VBandCombined_07DT_2020.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5425.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5375.msi', 'VVSSP-360S-F_SBandCombined_00DT_3550.msi', 'VVSSP-360S-F_VBandCombined_07DT_2100.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5450.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5400.msi', 'VVSSP-360S-F_SBandCombined_00DT_3575.msi', 'VVSSP-360S-F_VBandCombined_07DT_2110.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5475.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5425.msi', 'VVSSP-360S-F_SBandCombined_00DT_3600.msi', 'VVSSP-360S-F_VBandCombined_07DT_2132.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5500.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5450.msi', 'VVSSP-360S-F_SBandCombined_00DT_3625.msi', 'VVSSP-360S-F_VBandCombined_07DT_2155.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5525.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5475.msi', 'VVSSP-360S-F_SBandCombined_00DT_3650.msi', 'VVSSP-360S-F_VBandCombined_07DT_2170.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5550.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5500.msi', 'VVSSP-360S-F_SBandCombined_00DT_3675.ahb', 'VVSSP-360S-F_VBandCombined_07DT_2300.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5575.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5525.msi', 'VVSSP-360S-F_SBandCombined_00DT_3675.apb', 'VVSSP-360S-F_VBandCombined_07DT_2350.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5600.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5550.msi', 'VVSSP-360S-F_SBandCombined_00DT_3675.msi', 'VVSSP-360S-F_VBandCombined_07DT_2400.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5625.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5575.msi', 'VVSSP-360S-F_SBandCombined_00DT_3700.msi', 'VVSSP-360S-F_VBandCombined_07DT_2450.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5650.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5600.msi', 'VVSSP-360S-F_SBandCombined_00DT_3725.msi', 'VVSSP-360S-F_VBandCombined_07DT_2500.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5675.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5625.msi', 'VVSSP-360S-F_SBandCombined_00DT_3750.msi', 'VVSSP-360S-F_VBandCombined_07DT_2535.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5700.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5650.msi', 'VVSSP-360S-F_SBandCombined_00DT_3775.msi', 'VVSSP-360S-F_VBandCombined_07DT_2550.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5725.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5675.msi', 'VVSSP-360S-F_SBandCombined_00DT_3800.msi', 'VVSSP-360S-F_VBandCombined_07DT_2570.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5750.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5700.msi', 'VVSSP-360S-F_VBandCombined_07DT_1695.msi', 'VVSSP-360S-F_VBandCombined_07DT_2620.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5775.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5725.msi', 'VVSSP-360S-F_VBandCombined_07DT_1710.msi', 'VVSSP-360S-F_VBandCombined_07DT_2655.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5800.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5750.msi', 'VVSSP-360S-F_VBandCombined_07DT_1733.msi', 'VVSSP-360S-F_VBandCombined_07DT_2690.msi',
                   'VVSSP-360S-F_Port 10 -45_00DT_5825.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5775.msi', 'VVSSP-360S-F_VBandCombined_07DT_1755.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5850.msi',
                   'VVSSP-360S-F_Port 9 +45_00DT_5800.msi', 'VVSSP-360S-F_VBandCombined_07DT_1795.msi', 'VVSSP-360S-F_Port 10 -45_00DT_5875.msi', 'VVSSP-360S-F_Port 9 +45_00DT_5825.msi',
                   'VVSSP-360S-F_VBandCombined_07DT_1800.msi'}

VVSSP_freq_filename_dict = {}
# {ant.split('_')[3].split('.')[0]:ant for ant in VVSSP_filenames}
for file in VVSSP_filenames:
    freq = file.split('_')[3].split('.')[0]
    if freq in VVSSP_freq_filename_dict.keys():
        VVSSP_freq_filename_dict[freq].append(file)
    else:
        VVSSP_freq_filename_dict[freq] = []
        VVSSP_freq_filename_dict[freq].append(file)

VVSSP_freq_filename_dict = {key: VVSSP_freq_filename_dict[key] for key in sorted(VVSSP_freq_filename_dict.keys())}

# creation of the freq to filename relation for adding in as a file
AW3828_freq_filename_dict = {ant.split('_')[1]: ant for ant in AW3828_filenames}
AW3924_freq_filename_dict = {ant.split('_')[1]: ant for ant in AW3924_filenames}


# set the path split var to keep this working in windows
if os.name == 'nt':
    main_dir = os.path.abspath('..').split('\\')[-1]
else:
    main_dir = os.path.abspath('..').split('/')[-1]


def pull_paths_pkl(paths_file):
    if os.path.getsize(paths_file) == 0:
        logger.debug('file is empty')
        loaded_paths = []
    else:
        with open(paths_file, 'rb') as file:
            loaded_paths = pickle.load(file)
            print(f'loaded_paths:{loaded_paths}')
            loaded_paths = [str(Path(p)) for p in loaded_paths]
    print(f'loaded_paths after Path(p):{loaded_paths}')
    loaded_paths_joined = ''.join(loaded_paths)
    print(f"loaded_paths after join:{loaded_paths_joined}")
    loaded_paths_joined_split = loaded_paths_joined.split(';')
    print(f"loaded_paths after split:{loaded_paths_joined_split}")
    if loaded_paths_joined == '':
        confirm = False
    else:
        loaded_paths_joined_split = [path for path in loaded_paths_joined_split if path]
        print(f"loaded_paths after removing empties:{loaded_paths_joined_split}")
        confirm = questionary.confirm('would you like to use a saved path?').ask()
    if confirm:
        win_path = ''
        while win_path == '':
            path_selection = select_items('select a single path from the saved ones below:', loaded_paths_joined_split)
            if len(path_selection) != 1:
                print('please select a single path for the location of the project')
                win_path = ''
            else:
                win_path = path_selection[0]
        print(win_path)
    else:
        win_path = ''

    return loaded_paths_joined_split, win_path


def get_windows_path():
    # Prompt for project name and validate according to criteria
    try:
        paths_file = os.path.join('..', 'pickled_data', 'paths.pkl')
        saved_paths, win_path = pull_paths_pkl(paths_file)
        print(f'win_path:{win_path}')
        print(f'saved_paths:{saved_paths}')
        if win_path == '':
            name = ''
            while not re.match('^[A-Z][:0-9a-zA-Z_\\\]+$', name):
                name = input('please provide the abs path where this project will be located in the Windows system\n')
                logger.debug(f'name:{name}')
                print(f'name:{name}')
                print(f'Path(name):{Path(name)}')
                saved_paths.append(''.join(name))
                print(f'saved_paths:{saved_paths}')
                confirmation = questionary.confirm('this cannot be changed later and cannot be validated by this script. please check the path and verify that it is correct before continuing\n').ask()
                if confirmation:
                    with open(paths_file, 'wb') as file:
                        saved_paths = ';'.join(saved_paths)
                        print(f'saved_paths:{saved_paths}')
                        pickle.dump(saved_paths, file)
                    print(f'win_path from name:{name}')
                    return name
                else:
                    name = ''
        else:
            print(f'win_path:{win_path}')
            return win_path
    except Exception as e:
        logger.error(f'problem with getting the path for windowspath {e}')
        sys.exit(1)


def custom_exit(status):
    logger.warning(f'exiting from status:{status}')
    sys.exit(status)


# something I tried to hold all dir data, but working with it is a bit off from what I expect. will continue to attemp this style later on
def dir_tree_to_dict(start_path):
    tree_dict = {}
    for root, dirs, files in os.walk(start_path):
        tree_dict[f"..{root.split(main_dir)[1]}"] = {'dirs': dirs, 'files': files}
    return tree_dict


antennas_dict = dir_tree_to_dict(antenna_dir)


def handle_sigint(signum, frame):
    while True:
        try:
            selection = questionary.confirm('would you like to exit?').ask()
            if selection:
                logger.debug('user selected exit program. exiting')
                sys.exit(0)
            else:
                logger.debug('user selected no exit returning to execution.')
                break
        except Exception as e:
            logger.debug(f'error happened while handling sigint:\n{e}')


def get_project_name():
    # Prompt for project name and validate according to criteria
    name = ''
    while not re.match('^[0-9a-z_]+$', name):
        name = input('enter a name for the project.\nName must be lowercase, not contain symbols other than words, numbers, and be separated by underscores\n')
        logger.debug(f'name:{name}')
        projects = os.listdir(proj_path)
        if name in projects:
            logger.warning(f'project directory {name} already exists. choose a different name.')
            name = ''
    return name
    # Return the validated project name


def select_items(prompt, items):
    # if empty, return []
    if len(items) == 0:
        return []
    # Display the items and prompt the user to select by ticking boxes with an 'x'
    selection = questionary.checkbox(prompt, choices=items).ask()
    logger.debug(f'displaying res:{selection}')
    logger.debug('returning the selected items')
    return selection
    # Return the selected items


def create_project_directory(project_name):
    # Create a directory with the given project name
    logger.debug(f'creating project directory for {project_name} in {os.path.abspath(proj_path)}')
    os.mkdir(os.path.join(os.path.abspath(proj_path), project_name))
    return os.path.join(os.path.abspath(proj_path), project_name)
    # Return the directory path after creating it


def write_csv_with_pandas(file_path, data):
    # Write the given data to a CSV file using Pandas
    try:
        data.to_csv(file_path, index=False)
        logger.debug(f'writing to csv at path:{file_path}')
    except Exception as e:
        logger.error(f'problem saving the data to path:{file_path}')
        logger.warning(f'data:\n{data}')
        logger.critical(f'error:\n{e}')
        sys.exit(1)


def create_symlink(target_file, link_name):
    # Creat  a symlink to the target file with the given link name
    try:
        os.link(target_file, link_name)
        logger.debug(f'creating sym link for data:{target_file} using name:{link_name}')
    except Exception as e:
        logger.debug(f'error:{e}')
        sys.exit(1)


def pickle_project_data(project_data, cache_file):
    # Serialize the project data using pickling
    # with open(cache_file, 'wb') as handle:
    #    pickle.dump(project_data, handle, protocol=pickle.HIGHEST_PROTOCOL)
    logger.debug(f'predone: pickled project:{cache_file} from:{project_data}')


def list_existing_projects(pickle_dir):
    # List all pickled projects in the specified directory
    projects = [f[:-4] for f in os.listdir(pickle_dir) if f.endswith('_proj.pkl')]
    return projects


def load_project_from_pickle(project_name, pickle_dir):
    # Load the project data from the pickled file
    # with open(os.path.join(pickle_dir, f"{project_name}.pkl"), 'rb') as handle:
    #     project_data = pickle.load(handle)
    # return project_data
    logger.debug(f'predone: loading project {project_name} from:{pickle_dir}')
    return []
    # returns empty data until implemented


def link_routes(routes, proj_name):
    if len(routes) == 0:
        return
    os.mkdir(os.path.join(proj_path, proj_name, 'routes'))
    for route in routes:
        os.link(os.path.join(routes_path, f'{route}.csv'), os.path.join(proj_path, proj_name, f'routes/{route}.csv'))
        logger.debug(f"\ntarget:{os.path.join(routes_path, f'{route}.csv')}\nlink:{os.path.join(proj_path, f'{route}.csv')}")


def link_antennas(antenna_list, proj_name):
    if len(antenna_list) == 0:
        return
    contains_misc = 0
    new_ant_dir = os.path.join(proj_path, proj_name, 'antennas')
    os.mkdir(new_ant_dir)
    logger.debug(f"successfully made new dir for antennas at {new_ant_dir}")
    logger.debug(f"new antenna dir path for proj at:{new_ant_dir}")
    for ant in antenna_list:
        if 'VVSSP' in ant:
            link_path = os.path.abspath(os.path.join(f'{antennas_path}', f'{ant}','msi'))
            target_path = os.path.join(new_ant_dir, ant)
        elif 'AW3828' in ant:
            print(os.path.abspath(os.path.join(antennas_path, ant, 'MSI')))
            os.mkdir(os.path.join(new_ant_dir, ant,))
            link_path = os.path.abspath(os.path.join(antennas_path, ant, 'MSI'))
            target_path = os.path.join(new_ant_dir, ant, 'MSI')
        elif 'AW3924' in ant:
            print(os.path.abspath(os.path.join(antennas_path, ant, 'MSI')))
            link_path = os.path.abspath(os.path.join(antennas_path, ant, 'MSI'))
            target_path = os.path.join(new_ant_dir, ant)
        else:
            link_path = os.path.abspath(os.path.join(antennas_path, 'misc_antennas'))
            target_path = os.path.join(new_ant_dir, ant)
            contains_misc = 1
        print(f'linking {link_path} to {target_path}')
        os.symlink(link_path, target_path)
    if contains_misc:
        # fix this later to only include the msi files in this dir
        try:
            link_path = os.path.abspath(f'{antenna_dir}/misc_antennas')
            target_path = os.path.join(new_ant_dir, 'misc')
            print(f'linking {link_path} to {target_path}')
            print(os.path.abspath(os.path.join(f'{antennas_path}', f'{ant}')))
            os.symlink(link_path, target_path)
        except Exception as e:
            logger.debug(f'path for link:{antenna_dir}/misc_antennas already created.\n error:{e}')
    logger.debug('all antenna directories have been linked successfully')


def get_routes():
    # lists route files in the routes path to pull names.
    route_files = os.listdir(routes_path)
    route_files = [file.replace('.csv', '') for file in route_files if 'bakup' not in file]
    return route_files


# retuns a new frame that contains only the sites the user selected in the prompt
def collect_sites(sites):
    matches = []
    for site in sites:
        splitup = site.split(':')
        site_name = splitup[0].strip()
        site_type = splitup[1].strip()
        matching = deployment_sites.loc[(deployment_sites['Station'] == site_name) & (deployment_sites['Type'] == site_type)]
        matches.extend(matching.index.tolist())
    if len(matches) > 0:
        logger.debug('have matches')
        return deployment_sites.loc[matches]
    else:
        logger.debug('no matches found')
        return []


# pulls the site names from the deployment csv and lists the sites in nice readable format
def get_sites():
    logger.debug(f'\n{deployment_sites}')
    # Determine the maximum length of the station name and format output'
    max_length = max(len(kind) for kind in deployment_sites['Station'])
    sites = [f"{site:<{max_length+1}}:{kind}" for site, kind in zip(deployment_sites['Station'], deployment_sites['Type'])]
    logger.debug(sites)
    return sites


def get_antennas():
    antennas = []
    logger.debug("getting antennas from antennas_dict")
    for root in antennas_dict.keys():
        val = root.split('antennas')[1][1:].split('/')[0].replace('_', '')
        logger.debug(val)
        if 'msi-' in val:
            val = val.replace('msi-', '')
        elif 'antenna2d-' in val:
            val = val.replace('antenna2d-', '')
        elif 'misc' in val:
            logger.debug('\tmisc')
            logger.debug(antennas_dict[root]['files'])
        antennas.append(val)
    antennas = set(antennas)
    antennas.remove('')
    logger.debug(antennas)

    return antennas


def get_misc_antennas(selected_antennas):
    misc_antennas = []
    if 'misc' in selected_antennas:
        for element in antennas_dict['../antennas/misc_antennas']['files']:
            if 'msi' in element:
                misc_antennas.append(element.split('.')[0])
                logger.debug(misc_antennas)
        for val in selected_antennas:
            misc_antennas.append(val)
        logger.debug(f'misc_antennas:{misc_antennas}')
        misc_antennas.remove('misc')
        logger.debug(f'misc_antennas after misc removal:{misc_antennas}')
    return misc_antennas


global frequencies_so_far
frequencies_so_far = {}


def get_antenna_path_with_freq(windowspath, project_name, antenna):
    global frequencies_so_far
    confirmation = False
    if len(frequencies_so_far[antenna]) != 0:
        confirmation = questionary.confirm('reuse selected frequency?').ask()
    try:
        if antenna == 'AW3828':
            # AW3828 antenna setup
            logger.debug('AW3828 antenna recognized')
            if not confirmation:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', AW3828_freq_filename_dict.keys())
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        if (selected_freq[0] not in frequencies_so_far[antenna]):
                            frequencies_so_far[antenna].append(selected_freq[0])
                        break
            else:
                while True:
                    selected_freq = select_items('select one of the previously selected frequencies', frequencies_so_far[antenna])
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        break
            print(f'selected_freq:{selected_freq}')
            this_antenna_path = f"{windowspath}{project_name}\\antennas\\AW3828\\{AW3828_freq_filename_dict[selected_freq[0]]}"
            logger.debug(f'this antenna path = {this_antenna_path}')
            return this_antenna_path, selected_freq[0]

        elif antenna == 'AW3924':
            # AW3924 antenna setup
            logger.debug('AW3924 antenna recognized')
            while True:
                selected_port = select_items('Antenna has 4 ports, which one is of interest? select one', ['P1', 'P2', 'P3', 'P4'])
                if len(selected_port) != 1:
                    print('please select a single port since only one path can be selected for this antenna entry.')
                else:
                    break
            if not confirmation:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', AW3924_freq_filename_dict.keys())
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        if (selected_freq[0] not in frequencies_so_far[antenna]):
                            frequencies_so_far[antenna].append(selected_freq[0])
                        break
            else:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', frequencies_so_far[antenna])
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        break

            print(f'selected_freq:{selected_freq}')
            this_antenna_path = f"{windowspath}{project_name}\\antennas\\AW3924\\{AW3924_freq_filename_dict[selected_freq[0]]}"
            logger.debug(f'this antenna path = {this_antenna_path}')
            return this_antenna_path, selected_freq[0]

        elif antenna == 'VVSSP':
            # VVSSP-360S-F antenna setup
            logger.debug('VVSSP-360S-F antenna recognized')
            if not confirmation:
                while True:
                    selected_freq = select_items('Antenna has multiple frequencies, which one is of interest? select one', VVSSP_freq_filename_dict.keys())
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        if (selected_freq[0] not in frequencies_so_far[antenna]):
                            frequencies_so_far[antenna].append(selected_freq[0])
                        break
            else:
                while True:
                    selected_freq = select_items('select one of the previously selected frequencies', frequencies_so_far[antenna])
                    if len(selected_freq) != 1:
                        print('please select a single port freq. Only one file can be selected for this antenna entry.')
                    else:
                        break
            print(f'selected_freq:{selected_freq}')
            this_antenna_path = f"{windowspath}{project_name}\\antennas\\VVSSP-360S-F\\{VVSSP_freq_filename_dict[selected_freq[0]][0].split('.')[0]}.msi"
            logger.debug(f'this antenna path = {this_antenna_path}')
            return this_antenna_path, selected_freq[0]

        else:
            # some strategy for selecting the correct msi file for the misc antenna is needed. currently adds straightaway
            this_antenna_path = f"{windowspath}{project_name}\\{antennas_misc_dir}\\{antenna}.msi"
            print(f'this_antenna_path:{this_antenna_path}')
            return this_antenna_path, selected_freq[0]
    except Exception as e:
        logger.error(f'problem in get_antenna_path_with_freq({windowspath},{project_name},{antenna}):{e}')
        custom_exit(1)


def finalize_site_frame(frame: pd.DataFrame, sites, antennas, project_name) -> pd.DataFrame:
    global frequencies_so_far
    if frame.empty:
        logger.debug(f'frame is empty {frame}')
        return
    if len(sites) == 0:
        logger.debug(f'no sites provided {sites}')
        return
    if len(sites) == 0:
        logger.debug(f'no antennas provided {antennas}')
        return

    logger.debug(f'\nframe:{frame}')
    logger.debug(f'\nsites:{sites}')
    logger.debug(f'\nantennas:{antennas}')
    frame_copy = pd.DataFrame(frame).assign(frequency=0, power=0, az=0, el=0, sector=0, antpath='')
    windowspath = get_windows_path()
    print(f'windowspath:{windowspath}')
    site2sectors = {}
    for station in frame['Station']:
        sectors = -1
        while sectors < 0:
            try:
                sectors = input(f'how many sectors will {station} have?')
                sectors_int = int(sectors)
                logger.debug(f'sectors correct sectors:{sectors_int}')
                sectors = sectors_int
            except ValueError:
                logger.debug(f'sectors must be an integer. entered:{sectors}')
                sectors = -1
                continue
            if sectors_int > 4:
                logger.debug('max sectors is 4 for now. setting to 4')
                sectors = 4
                sectors_int = sectors
                site2sectors[station] = sectors_int
                break
            elif sectors_int < 1:
                logger.debug('sectors must be greater than 0 and less than 5')
                sectors = -1
            else:
                site2sectors[station] = sectors_int
        logger.debug(f'site:{station}\nsite2sectors:{site2sectors}')

    for site in site2sectors.keys():
        logger.debug(f'range(site2sectors[{site}]):{list(range(site2sectors[site]))}')
        for i in range(site2sectors[site]):
            while True:
                antenna = select_items(f'chose an antenna for the {site} site', antennas)
                if len(antenna) != 1:
                    logger.debug('please select one antenna')
                else:
                    break
            logger.debug(f'adding in sector {i} with antenna {antenna[0]} to {site}')
            logger.debug(f"{windowspath}{antenna[0]}")
            row_to_duplicate = frame_copy[(frame_copy['Station'] == site)]
            logger.debug(f"first copy:\n{row_to_duplicate}")
            row_to_duplicate = row_to_duplicate[row_to_duplicate['sector'] == 0].copy()
            logger.debug(f"copy of copy made:\n{row_to_duplicate}")
            row_to_duplicate['sector'] = i + 1
            row_to_duplicate['Station'] = f"{site}{i + 1}"
            logger.debug(f"sector added:\n{row_to_duplicate}")
            logger.debug(f'site2sectors[site]:\n{site2sectors[site]}')
            row_to_duplicate['az'] = i * 360.0 / site2sectors[site]
            logger.debug(f"az added:\n{row_to_duplicate}")
            row_to_duplicate['el'] = 0
            logger.debug(f"el added:\n{row_to_duplicate}")
            # path needs to be Joined as the path for windows with project name added as well. also ask user about freq for the site
            selected_port_path, freq = get_antenna_path_with_freq(windowspath, project_name, antenna[0])
            logger.debug(f'new path = {selected_port_path}')
            row_to_duplicate['antpath'] = selected_port_path
            row_to_duplicate['frequency'] = freq
            row_to_duplicate['power'] = 36  # need to convert to ask user for a power level, or take from parameters
            logger.debug(f"ant path added:\n{row_to_duplicate}")
            frame_copy = pd.concat([frame_copy, row_to_duplicate]).reset_index(drop=True)
    rows_to_drop = frame_copy[frame_copy['sector'] == 0].index
    frame_copy = frame_copy.drop(rows_to_drop)
    logger.debug(f"final frame dropped duplicates and all info added except elevation and power:\n{frame_copy}")
    # next is the setup of the radio info. associate the radio to a power level and set power column
    logger.debug(f'\nframe sent back\n:{frame_copy}')
    logger.debug(f'windows path provided:{windowspath}')
    # change the name of all columns to match feko expectations
    # Rename columns
    frame_copy.rename(columns={
        'sector': 'Number',
        'az': 'Azimuth',
        'el': 'Downtilt',
        'Station': 'Name',
        'UTM_Easting': 'Longitude',
        'UTM_Northing': 'Latitude',
        'antpath': 'AntennaPattern',
        'Height (m)': 'Height'
    }, inplace=True)
    frame_copy.drop(['Latitude (deg N)', 'Longitude (deg E)'], axis=1, inplace=True)  # This will drop columns with names 'Col1' and 'Col2'.
    logger.debug(f"final frame:\n{frame_copy}")
    return frame_copy


def main():
    parser = argparse.ArgumentParser(description='Your program description here')

    # Add options
    parser.add_argument('-D', '--debug', action='store_true', help='Enable debugging')
    parser.add_argument('-f', '--file', type=str, help='Specify a JSON filename - not implemented yet')
    args = parser.parse_args()
    if args.debug:
        CustomFormatter.setLogLevel(logger, logging.DEBUG)
    else:
        CustomFormatter.setLogLevel(logger, logging.INFO)

    if args.file:
        if not re.match('[a-zA-Z0-9_.]*.json', args.file):
            print(f'filename not provided correctly. {args.file} is not a proper json filename. Please provide filename if using the -f flag\n')
            custom_exit(1)
        logger.debug(f"Filename is {args.file}")
    else:
        logger.debug('filename not provided.\n')

    global frequencies_so_far
    try:
        project_name = get_project_name()
        # obtain routes from csv and sites from csv to ask user
        route_set = get_routes()
        site_set = get_sites()
        antennas = get_antennas()
        logger.debug("routes, sites, antennas pulled successfully")
        selected_routes = select_items("Choose routes:", route_set)
        selected_sites = select_items("Choose sites:", site_set)
        selected_antennas = select_items("Choose antennas:", antennas)
        if 'misc' in selected_antennas:
            all_antennas = get_misc_antennas(selected_antennas)
            all_antennas = select_items("Misc antennas added. Select again:", all_antennas)
        else:
            all_antennas = selected_antennas

        for ant in all_antennas:
            frequencies_so_far[ant] = []
        logger.debug(f'antennas:{all_antennas}')
        map_choice = select_items("Choose map:", ["Premade Campus Map", "Other Custom Map"])
        logger.debug(f'map_choice:{map_choice}')
        if map_choice[0] == "Premade Campus Map":
            # create project directory in projects and place files in there
            project_dir = create_project_directory(project_name)
            new_siteframe = collect_sites(selected_sites)
            logger.debug(f'new_siteframe:\n{new_siteframe}')
            logger.debug(f'project_dir:{project_dir}')
            logger.debug(f'routes:{selected_routes}\nsites:{selected_sites}\nmap:{map_choice}')
            # create sym links for the routes selected and add to the project
            link_routes(selected_routes, project_name)
            # add antennas to site frame if chosen
            if len(all_antennas) != 0:
                logger.debug('antennas not empty')
                link_antennas(all_antennas, project_name)
            else:
                logger.debug('antennas looks empty')
                logger.debug(all_antennas)
            # map is always in this location. link to it and add to directory
            create_symlink("../mapdata/map_UofU_campus_outputfile.osm", os.path.join(project_dir, "UofU_osm_map.osm"))
        else:
            while True:
                map_path = input('please provide a path for the map you will use.\n')
                try:
                    create_symlink(map_path, os.path.join(project_dir, "Custom_map.osm"))
                    logger.debug(f'map path: {map_path}')
                    break
                except Exception as e:
                    logger.error(f'error occured in symlinking of custom map. check the path provided and try again\nexception:{e}')
        logger.debug('all files linked successfully. Creating sites csv')
        # write the sites file to the project
        logger.warning('\n\nuntil I find a way to associate a site to a selected antenna, antennas will need to be added manually\n\n')
        final_frame = finalize_site_frame(new_siteframe, selected_sites, all_antennas, project_name)
        write_csv_with_pandas(os.path.join(project_dir, "sites.csv"), final_frame)
    except Exception as e:
        logger.error(f'problem with creation process from:{e}')
        sys.exit(1)

    # TODO:
    # copy files from the base files dir and build up the net, nup, wst, mic files for this project
    # read in dir and go one by one on the dirs building up the files
    # create the file and write to proj_dir
    # test execution of feko winprop with the project built

    logger.info("Project setup complete!")



def validate_and_get_map(config):
    map_choice = config.get('map')
    if map_choice not in ["Premade Campus Map", "Other Custom Map"]:
        raise ValueError(f'Invalid map choice: {map_choice}')
    return map_choice

def validate_and_get_project_name(config):
    project_name = config.get('project_name')
    if not re.match('[a-zA-Z0-9_]*', project_name):
        raise ValueError(f'Invalid project name: {project_name}')
    return project_name

# Similar functions for routes, sites, antennas, sectors, frequencies
def validate_and_get_routes(config):
    # Validation logic here
    pass

def validate_and_get_sites(config):
    # Validation logic here
    pass

def validate_and_get_antennas(config):
    # Validation logic here
    pass

def validate_and_get_sectors(config):
    # Validation logic here
    pass

def validate_and_get_frequencies(config):
    # Validation logic here
    pass

def file_option_main():
    parser = argparse.ArgumentParser(description='Your program description here')

    # Add options
    parser.add_argument('-D', '--debug', action='store_true', help='Enable debugging')
    parser.add_argument('-f', '--file', type=str, help='Specify a JSON filename - not implemented yet')
    args = parser.parse_args()
    if args.debug:
        CustomFormatter.setLogLevel(logger, logging.DEBUG)
    else:
        CustomFormatter.setLogLevel(logger, logging.INFO)

    if args.file:
        if not re.match('[a-zA-Z0-9_.]*.json', args.file):
            print(f'filename not provided correctly. {args.file} is not a proper json filename. Please provide filename if using the -f flag\n')
            custom_exit(1)
        logger.debug(f"Filename is {args.file}")
    else:
        logger.debug('filename not provided.\n')
        custom_exit(1)

    # Read the JSON config file
    config_path = args.file
    with open(config_path, 'r') as f:
        config = json.load(f)
        
    try:
        project_name = validate_and_get_project_name(config)
        map_choice = validate_and_get_map(config)
        routes = validate_and_get_routes(config)
        sites = validate_and_get_sites(config)
        antennas = validate_and_get_antennas(config)
        sectors = validate_and_get_sectors(config)
        frequencies = validate_and_get_frequencies(config)
    except ValueError as e:
        logger.warning(f'Validation error: {e}')
        custom_exit(1)
    
    # TODO:
    # copy files from the base files dir and build up the net, nup, wst, mic files for this project
    # read in dir and go one by one on the dirs building up the files
    # create the file and write to proj_dir
    # test execution of feko winprop with the project built


if __name__ == "__main__":
    signal.signal(signal.SIGINT, handle_sigint)
    if file_option:
        file_option_main()
    else:
        main()
