import argparse
import contextily as ctx
import geopandas as gpd
import logging
import math
import matplotlib
import matplotlib.colors as mcolors
import matplotlib.pyplot as plt
import numpy as np
import os
import pandas as pd
from pyproj import Proj, Transformer, Geod
import rasterio
from rasterio.transform import from_origin
import sys

LOG = logging.getLogger(__name__)


def calculate_resolution(resolution_meters, lower_left_lat, upper_right_lat):
    """
    Convert resolution from meters (UTM) to degrees (WGS84).

    :param resolution_meters: Resolution in meters (from UTM coordinates).
    :param lower_left_lat: Latitude of the lower left point in degrees.
    :param upper_right_lat: Latitude of the upper right point in degrees.
    :return: Resolution in degrees.
    """

    # Convert resolution for latitude (constant across all latitudes)
    resolution_lat = resolution_meters / 111000  # 111 kilometers per degree

    # Calculate average latitude for converting longitude resolution
    average_latitude = (lower_left_lat + upper_right_lat) / 2

    # Convert resolution for longitude (varies with latitude)
    resolution_lon = resolution_meters / (111000 * math.cos(math.radians(average_latitude)))

    # Use the smaller of the two resolutions for uniformity
    final_resolution = min(resolution_lat, resolution_lon)

    return final_resolution


# Function to parse the data file
def parse_data_file(filepath):
    try:
        LOG.debug(f"filepath: {filepath}")
        with open(filepath, 'r') as file:
            data_started = False
            lower_left = upper_right = None
            resolution = None
            data = []
            for line in file:
                # LOG.debug(f"line: {line}")
                if line.startswith('LOWER_LEFT'):
                    LOG.debug(f"line: {line}")
                    parts = line.strip().split()
                    LOG.debug(f"parts: {parts}")
                    if len(parts) >= 3:
                        lower_left_x, lower_left_y = map(float, parts[1:3])
                        lower_left = (lower_left_x, lower_left_y)
                elif line.startswith('UPPER_RIGHT'):
                    LOG.debug(f"line: {line}")
                    parts = line.strip().split()
                    LOG.debug(f"parts: {parts}")
                    if len(parts) >= 3:
                        upper_right_x, upper_right_y = map(float, parts[1:3])
                        upper_right = (upper_right_x, upper_right_y)
                elif line.startswith('RESOLUTION'):
                    parts = line.strip().split()
                    if len(parts) >= 2:
                        resolution = float(parts[1])
                elif line.startswith('BEGIN_DATA'):
                    data_started = True
                    continue
                if data_started:
                    parts = line.strip().split()
                    if len(parts) == 3 and 'N.C.' not in parts:
                        x, y, power = map(float, parts)
                        data.append({'x': x, 'y': y, 'power': power})
        return pd.DataFrame(data), lower_left, upper_right, resolution

    except Exception as e:
        LOG.debug(f"error {e}. exiting from parse data...")
        return e


# geotif creator for data with utm coordinates 
def create_geotiff(data, lower_left_utm, upper_right_utm, resolution_m, filename):
    try:
        utm_zone = 12
        northern = True
        # Calculate number of columns and rows using UTM coordinates
        num_cols = int((upper_right_utm[0] - lower_left_utm[0]) / resolution_m)
        num_rows = int((upper_right_utm[1] - lower_left_utm[1]) / resolution_m)

        # Initialize grid
        grid = np.full((num_rows, num_cols), np.nan)  # Using NaN for empty cells

        # Populate the grid
        for index, row in data.iterrows():
            # Convert lat/long to UTM coordinates if data is in lat/long
            # utm_x, utm_y = transform_latlong_to_utm(row['Latitude'], row['Longitude'])
            # Use these UTM coordinates for grid placement
            utm_x, utm_y = row['x'], row['y']  # Assuming UTM coordinates are in the data
            grid_x = int((utm_x - lower_left_utm[0]) / resolution_m)
            grid_y = int((utm_y - lower_left_utm[1]) / resolution_m)

            if 0 <= grid_x < num_cols and 0 <= grid_y < num_rows:
                grid[grid_y, grid_x] = row['power']

        # Set up the transformation for GeoTIFF
        lower_left_wgs84 = convert_to_wgs84(lower_left_utm[0], lower_left_utm[1], utm_zone, northern)
        upper_right_wgs84 = convert_to_wgs84(upper_right_utm[0], upper_right_utm[1], utm_zone, northern)
        # Assuming lower_left_wgs84 and upper_right_wgs84 are correctly defined
        # upper_left_wgs84 = [lower_left_wgs84[0], upper_right_wgs84[1]]

        # Calculate midpoint latitude for accurate longitude resolution
        # Assuming you have lower_left_wgs84 and upper_right_wgs84 as the bounding coordinates
        mid_latitude = (lower_left_wgs84[1] + upper_right_wgs84[1]) / 2
        mid_longitude = (lower_left_wgs84[0] + upper_right_wgs84[0]) / 2
        geod = Geod(ellps="WGS84")
        # _, _, distance_per_longitude_degree = geod.inv(lower_left_wgs84[0], mid_latitude, lower_left_wgs84[0] + 1, mid_latitude)
        # Calculate distance for one degree of longitude at the midpoint latitude
        _, _, distance_per_longitude_degree = geod.inv(mid_longitude - 1, mid_latitude, mid_longitude, mid_latitude)
        # Calculate distance for one degree of latitude at the midpoint longitude
        _, _, distance_per_latitude_degree = geod.inv(mid_longitude, mid_latitude - 1, mid_longitude, mid_latitude)
        # Calculate resolutions
        resolution_x = resolution_m / distance_per_longitude_degree  # Resolution in degrees for longitude
        resolution_y = resolution_m / 111000  # Resolution in degrees for latitude (approximately constant)
        # Adjust transformation
        transform = from_origin(lower_left_wgs84[0], lower_left_wgs84[1], resolution_x, -resolution_y)

        # Write the data to a GeoTIFF
        with rasterio.open(filename, 'w', driver='GTiff',
                           height=num_rows, width=num_cols,
                           count=1, dtype=str(grid.dtype),
                           crs='EPSG:4326',  # WGS84 coordinate system
                           transform=transform) as dst:
            dst.write(grid, 1)
        LOG.debug(f"wrote {filename} successfully")
        LOG.debug(f"wrote {filename} successfully")
        return 0, ""
    except Exception as e:
        # LOG.debug(f"error {e}. exiting from geotiff creator...")
        return -1, f"error {e}. exiting from geotiff creator..."


# Function to create a geotiff file with lat, long coordinates
def create_geotiff2(data, lower_left, upper_right, resolution_m, filename):
    try:
        LOG.debug(f"filename: {filename}")
        LOG.debug(f"data: {data}")
        LOG.debug(f"lower_left: {lower_left}")
        LOG.debug(f"upper_right: {upper_right}")
        LOG.debug(f"resolution in meters: {resolution_m}")

        # Convert resolution from meters to degrees
        resolution = resolution_m / 111320  # Approximate conversion at equator, consider more precise conversion based on latitude

        # Calculate number of columns and rows
        num_cols = int((upper_right[0] - lower_left[0]) / resolution)
        num_rows = int((upper_right[1] - lower_left[1]) / resolution)

        # Initialize grid
        grid = np.full((num_rows, num_cols), np.nan)  # Using NaN for empty cells

        LOG.debug(f"num_cols: {num_cols}")
        LOG.debug(f"num_rows: {num_rows}")

        # Populate the grid
        for index, row in data.iterrows():
            # Adjusted calculations for latitude and longitude
            grid_x = int((row['Longitude'] - lower_left[0]) / resolution)
            grid_y = num_rows - 1 - int((row['Latitude'] - lower_left[1]) / resolution)

            if 0 <= grid_x < num_cols and 0 <= grid_y < num_rows:
                grid[grid_y, grid_x] = row['power']
            else:
                LOG.debug(f"Skipping out-of-bounds data at index {index} with coordinates ({row['Longitude']}, {row['Latitude']})")

        # Set up the transformation
        transform = from_origin(lower_left[0], upper_right[1], resolution, -resolution)

        # Write the data to a GeoTIFF
        with rasterio.open(filename, 'w', driver='GTiff',
                           height=num_rows, width=num_cols,
                           count=1, dtype=str(grid.dtype),
                           crs='EPSG:4326', transform=transform) as dst:
            dst.write(grid, 1)

        return 0, ""
    except Exception as e:
        # LOG.debug(f"error {e}. exiting from geotiff creator...")
        return -1, f"error {e}. exiting from geotiff creator..."


# Function to create GeoTIFF UTM
def create_UTM_geotiff(data, lower_left, upper_right, resolution, filename):
    try:
        LOG.debug(f"filename: {filename}")
        LOG.debug(f"data: {data}")
        LOG.debug(f"lower_left: {lower_left}")
        LOG.debug(f"upper_right: {upper_right}")
        LOG.debug(f"resolution: {resolution}")
        num_cols = int((upper_right[0] - lower_left[0]) / resolution)
        num_rows = int((upper_right[1] - lower_left[1]) / resolution)
        grid = np.full((num_rows, num_cols), np.nan)  # Using NaN for empty cells
        LOG.debug(f"num_cols: {num_cols}")
        LOG.debug(f"num_rows: {num_rows}")
        for index, row in data.iterrows():
            grid_x = int((row['x'] - lower_left[0]) / resolution)
            grid_y = num_rows - 1 - int((row['y'] - lower_left[1]) / resolution)
            if 0 <= grid_x < num_cols and 0 <= grid_y < num_rows:
                grid[grid_y, grid_x] = row['power']
            else:
                LOG.debug(f"Skipping out-of-bounds data at index {index} with coordinates ({row['x']}, {row['y']})")

        transform = from_origin(lower_left[0], upper_right[1], resolution, resolution)
        
        with rasterio.open(filename, 'w', driver='GTiff',
                           height=num_rows, width=num_cols,
                           count=1, dtype=str(grid.dtype),
                           crs='EPSG:32612', transform=transform) as dst:
            dst.write(grid, 1)
        return 0, ""
    except Exception as e:
        # LOG.debug(f"error {e}. exiting from geotiff creator...")
        return -1, f"error {e}. exiting from geotiff creator..."


# create a utm projector 
def get_utm_proj(utm_x, utm_y):
    utm_zone = 12  # Utah UTM zone
    hemisphere = 'north' if utm_y > 0 else 'south'
    
    utm_proj = Proj(proj='utm', zone=utm_zone, ellps='WGS84', south=hemisphere == 'south')
    return utm_proj


# convert single coord to wgs from utm 
def convert_to_wgs84(utm_x, utm_y, utm_zone, northern):
    # Determine the EPSG code for the UTM zone
    hemisphere_prefix = '326' if northern else '327'
    utm_epsg = f"{hemisphere_prefix}{utm_zone}"

    # Create transformer objects
    transformer_to_wgs84 = Transformer.from_crs(f"EPSG:{utm_epsg}", "EPSG:4326", always_xy=True)

    # Perform the transformation
    lon, lat = transformer_to_wgs84.transform(utm_x, utm_y)
    return lon, lat


# convert a vectorized set of coords to wgs from utm
def convert_to_wgs84_vectorized(x_coords, y_coords, utm_zone, northern):
    hemisphere_prefix = '326' if northern else '327'
    utm_epsg = f"{hemisphere_prefix}{utm_zone}"

    transformer_to_wgs84 = Transformer.from_crs(f"EPSG:{utm_epsg}", "EPSG:4326", always_xy=True)

    # Transform arrays of coordinates
    lon, lat = transformer_to_wgs84.transform(x_coords, y_coords)
    return lon, lat


# makes an image for a passed file and converts utm to wgs in this setting
def image_maker(in_file):
    LOG.debug(f"inside image_maker with args:{in_file}")
    try:
        # Step 1: Read data
        try:
            data, lower_left, upper_right, resolution_m = parse_data_file(in_file)
        except Exception as e:
            LOG.debug(f"error occured in parsing of data file: {e}")
            return "", f"error occured in parsing of data file: {e}"
        utm_zone = 12
        north = True
        data['Longitude'], data['Latitude'] = convert_to_wgs84_vectorized(data['x'].to_numpy(), data['y'].to_numpy(), utm_zone, north)
        # data = data.drop(columns=['x', 'y'])
        LOG.debug(f"data: {data}")
        upper_right_lon, upper_right_lat = convert_to_wgs84(upper_right[0], upper_right[1], utm_zone, north)
        lower_left_lon, lower_left_lat = convert_to_wgs84(lower_left[0], lower_left[1], utm_zone, north)
        lower_left_wgs = [lower_left_lon, lower_left_lat]
        upper_right_wgs = [upper_right_lon, upper_right_lat]
        LOG.debug(f"upper_right: {upper_right_wgs}")
        LOG.debug(f"lower_left: {lower_left_wgs}")
        resolution_d = calculate_resolution(resolution_m, lower_left_lat, upper_right_lat)
        gdf = gpd.GeoDataFrame(data, geometry=gpd.points_from_xy(data.Longitude, data.Latitude), crs="EPSG:4326")
        LOG.debug(f"lower_left: {lower_left}")
        LOG.debug(f"resolution_d: {resolution_d}")
        LOG.debug(f"resolution_m: {resolution_m}")
        # Step 2:
        fig, ax = plt.subplots(figsize=(10, 10))
        gdf.plot(ax=ax, column='power', markersize=.3, alpha=.8)
        ctx.add_basemap(ax, crs=gdf.crs.to_string())

        # Create a ScalarMappable with the same colormap and norm
        norm = mcolors.Normalize(vmin=gdf['power'].min(), vmax=gdf['power'].max())
        cmap = matplotlib.colormaps['viridis']
        sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
        sm.set_array([])

        file_directory = os.path.dirname(in_file)
        if '/' in in_file:
            split_path = in_file.split('/')
            filename = f"{split_path[-1].split('.')[0]}"
        else:
            filename = in_file

        LOG.debug(f"filename: {filename}")

        # Adding title and axis labels
        ax.set_title(f"{filename} map")
        ax.set_xlabel("Longitude")
        ax.set_ylabel("Latitude")
        # Manually create and adjust the legend
        handles, labels = ax.get_legend_handles_labels()
        legend = ax.legend(handles, labels, loc='upper left', title='Power', bbox_to_anchor=(1.5, 2))
        legend.get_title().set_fontsize('10')  # Adjust legend title font size
        # Create a colorbar with more control
        cbar = fig.colorbar(sm, ax=ax, fraction=0.02, pad=0.04)
        cbar.set_label('Power')
        # Step 3: Exporting map as image
        new_filename_png = os.path.join(file_directory, f"{filename}_map.png")
        LOG.debug(f"new_filename_png: {new_filename_png}")
        plt.savefig(new_filename_png, format='png')
        # sys.exit(1)
        LOG.debug(f"wrote {new_filename_png} successfully")
        LOG.debug(f"wrote {new_filename_png} successfully")
        # Step 4: Creating and exporting as GeoTIFF
        LOG.debug("calling geotiff creator:")
        new_filename_tif = os.path.join(file_directory, f"{filename}_data.tif")
        LOG.debug(f"new_filename_tif: {new_filename_tif}")
        exit_status, msg = create_geotiff(data, lower_left, upper_right, resolution_m, new_filename_tif)
        # return error and message if there is a problem
        if exit_status:
            LOG.debug(f"error message from create_geotiff:\n{msg}")
            return "", f"error occured in creating the geotif file: {msg}"
        # otherwise, return new filename for created tif image
        return new_filename_tif, ""

    except Exception as e:
        LOG.debug(f"error {e} from file {in_file}. exiting...")
        return "", f"error {e} from file {in_file}."


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='script for building up a project for feko', formatter_class=argparse.RawTextHelpFormatter)
    # Add options
    parser.add_argument('-D', '--debug', action='store_true', help='Enable debugging')
    parser.add_argument('-f', '--file', type=str, nargs='+', help='Specify a file to parse and make a map, or multiple files all expected to be Power.txt files')
    args = parser.parse_args()

    if os.path.isdir(args.file[0]):
        LOG.debug(f"dir: {args.file[0]}")
        path = args.file[0]
        args.file = os.listdir(os.path.abspath(args.file[0]))
        args.file = [f"{path}{filepath}" for filepath in args.file]
        args.file = [f"{filepath}" for filepath in args.file if filepath.endswith('.txt')]
        # LOG.debug(args.file)
        # sys.exit(1)
    if args.file is not None:
        if len(args.file) > 1:
            for file in args.file:
                if '.txt' not in file:
                    LOG.debug(f"Error, args.file: {file} must be a txt file format")
                    sys.exit(1)
            for file in args.file:
                LOG.debug(f"file {file} contains txt. running main")
                image_path, err = image_maker(file)
                if err != "":
                    LOG.debug(f"error occured in creator\nerr: {err}\n exiting")
                    sys.exit(1)
        else:
            LOG.debug(f"single file {args.file[0]} contains txt. running main")
            image_path, err = image_maker(args.file[0])
            if err != "":
                LOG.debug(f"error occured in creator\nerr: {err}\n exiting")
                sys.exit(1)

    else:
        LOG.debug('no files provided. exiting..')
