#!/bin/bash

# Check if the correct number of arguments were provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 [directory] [output_csv_name]"
    exit 1
fi

# Directory passed as the first argument
directory=$1
# Output CSV file
output_csv="${directory}/"$2

# Write CSV header
echo "filepath,filename,frequency,beamwidth,gain,frequency_band" > "$output_csv"

find "$directory" -name '*.msi' -maxdepth 1 | while IFS= read -r filepath; do
    # Get the absolute path
    abs_path=$(realpath "$filepath" | tr -d '\r')

    # Extract the base name without the file extension
    filename=$(basename "$filepath" .msi | tr -d '\r')

    # Extract other fields and remove carriage returns
    frequency=$(sed -n '2p' "$filepath" | cut -d ' ' -f 2 | tr -d '\r')
    beamwidth=$(sed -n '3p' "$filepath" | cut -d ' ' -f 2 | tr -d '\r')
    gain=$(sed -n '4p' "$filepath" | cut -d ' ' -f 2 | tr -d '\r')
    frequency_band=$(sed -n '7p' "$filepath" | cut -d ' ' -f 2- | tr -d '\r')

    # Write to CSV with quoted variables
    echo "\"$abs_path\",\"$filename\",\"$frequency\",\"$beamwidth\",\"$gain\",\"$frequency_band\"" >> "$output_csv"
done
