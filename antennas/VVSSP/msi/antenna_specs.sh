#!/bin/bash

# Check if the correct number of arguments were provided
if [ "$#" -ne 2 ]; then
    echo "Usage: $0 [directory] [output_csv_name]"
    exit 1
fi

# Directory passed as the first argument
directory=$1
# Output CSV file
output_csv="${directory}/"$2

# Write CSV header
echo "filepath,filename,frequency,beamwidth,gain,frequency_band" > "$output_csv"

find "$directory" -name '*.msi' -maxdepth 1 | while IFS= read -r filepath; do
    # Extract the base filename without the file extension
    filename=$(basename "$filepath" .msi)
    # Extract other fields
    frequency=$(sed -n '2p' "$filepath" | cut -d ' ' -f 2)
    beamwidth=$(sed -n '3p' "$filepath" | cut -d ' ' -f 2)
    gain=$(sed -n '4p' "$filepath" | cut -d ' ' -f 2)
    frequency_band=$(sed -n '7p' "$filepath" | cut -d ' ' -f 2-)
    filepath=$(realpath "$filepath")
    
    # Write to CSV
    echo "$filepath,$filename,$frequency,$beamwidth,$gain,$frequency_band" >> "$output_csv"
done
