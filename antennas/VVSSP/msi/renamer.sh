find . -maxdepth 1 -name '*VVSSP*' | while IFS= read -r file; do
  newname=$(echo "$file" | tr ' ' '_')  # Replace spaces with underscores in the filename
  echo "file   : "$file
  echo "newname: "$newname
  # Rename the file
  mv "$file" "$newname"
done
